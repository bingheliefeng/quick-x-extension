--
-- Author: zhouzhanglin
-- Date: 2014-10-09 09:31:46
--
local Component = class("Component",function() 
    return display.newNode()
end)

function Component:ctor()

    self.enableMultiTouch = false -- 默认不支持多点触摸

    self._enable = true
    self._isRealClick = false
    self._touchDelta = cc.p(0,0)

    self.touchBound=nil --手动触摸区域

    self:addNodeEventListener(cc.NODE_TOUCH_EVENT , handler(self, self._touchHandler) )
end

function Component:_touchHandler(event)
    local name,x,y,prevX,prevY = event.name,event.x,event.y,event.prevX,event.prevY
    event.target = self
    if name=="began" then
        if not self._enable then return false end

        if self.touchBound~=nil then
            local localP = self:convertToNodeSpace(cc.p(x,y))
            if not cc.rectContainsPoint(self.touchBound,localP) then
                return false
            end
        end

        self._touchDelta.x = x
        self._touchDelta.y = y
        self._isRealClick = true

        if self._onPressFun ~= nil then
            self._onPressFun(event)
        end
        return true
    elseif name=="moved" then
        if self._onMoveFun ~= nil then
            self._onMoveFun(event)
        end
        if self._isRealClick==true and cc.pGetDistance(cc.p(x,y),self._touchDelta)>display.height/640*7 then
            self._isRealClick = false
        end
    elseif name=="ended" or name=="cancelled" then
        if self._onReleaseFun ~= nil then
            self._onReleaseFun(event)
        end
        if self:hitTestPoint(x,y) then
            if self._onClickFun ~=nil then
                self._onClickFun(event)
            end
            if self._onRealClickFun~= nil and self._isRealClick==true and cc.pGetDistance(cc.p(x,y),self._touchDelta)<display.height/640*7 then
                self._onRealClickFun(event)
            end
        elseif self._onReleaseOutFun~=nil then
            self._onReleaseOutFun(event)
        end
    end
end

function Component:onClick( fun )
    self._onClickFun = fun
    return self
end
function Component:onRealClick( fun )
    self._onRealClickFun = fun 
    return self
end
function Component:onRelease( fun )
    self._onReleaseFun = fun 
    return self
end
function Component:onReleaseOut( fun )
    self._onReleaseOutFun = fun 
    return self
end
function Component:onPress( fun )
    self._onPressFun = fun 
    return self
end
function Component:onMove( fun )
    self._onMoveFun = fun 
    return self
end
function Component:onBecameVisible( fun )
    self._onBecameVisibleFun = fun
    return self
end
function Component:onBecameInVisible( fun )
    self._onBecameInVisibleFun = fun
    return self
end




function Component:getChildAt(index)
    return self:getChildren()[index]
end


function Component:getChildIndex( child )
    for i, var in ipairs(self:getChildren()) do
    	if var==child then
    	   return i
    	end
    end
    return 0
end



----重写UI位置方法

function Component:setUIPosition(x,y)
    self:setPosition(x, -y)
end
function Component:getUIPosition()
    return cc.p( self:getPositionX(),-self:getPositionY() )
end
function Component:setUIPositionX(x)
    self:setPositionX(x)
    return self
end
function Component:getUIPositionX()
    self:getPositionX()
end
function Component:setUIPositionY(y)
    self:setPositionY(-y)
    return self
end
function Component:getUIPositionY()
    self:getPositionY()
end
function Component:setWidth(width)
    self:setScaleX( width/self:getContentSize().width)
    return self
end
function Component:setHeight(height)
    self:setScaleY( height/self:getContentSize().height)
    return self
end
function Component:setSize( width,height)
    self:setScaleX( width/self:getContentSize().width)
    self:setScaleY(  height/self:getContentSize().height)
    return self
end

--最后调用
function Component:drawBoundingBox(color , parent)
    local cbb = self:getBoundingBox()
    local left, bottom, width, height = cbb.x, cbb.y, cbb.width, cbb.height
    local points = {
        {left, bottom},
        {left + width, bottom},
        {left + width, bottom + height},
        {left, bottom + height},
        {left, bottom},
    }
    local box = display.newPolygon(points, {borderWidth=3})
    box:setLineColor(color)
    parent:addChild(box, 1000)
end


function Component:hitTestPoint(x,y)
    return self:getCascadeBoundingBox():containsPoint(cc.p(x,y))
end


function Component:setEnable(flag)
    self._enable = flag
end
function Component:getEnable()
    return self._enable
end


function Component:dispose()
    self._touchDelta = nil
    self._onReleaseOutFun = nil 
    self._onClickFun = nil 
    self._onRealClickFun = nil 
    self._onReleaseFun = nil 
    self._onPressFun = nil 
    self._onMoveFun = nil 
    self._onBecameVisibleFun = nil
    self._onBecameInVisibleFun = nil
end

return Component