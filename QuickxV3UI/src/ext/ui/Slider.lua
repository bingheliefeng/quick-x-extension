--
-- Author: zhouzhanglin
-- Date: 2014-10-10 10:24:35
--
----@module ext.ui.Slider 滑块
local Slider = class("Slider",require("ext.ui.Component"))
Slider.DIRECTION_HORIZONTAL = 1
Slider.DIRECTION_VERTICAL = 2

--------------------------------------------------------
----构造函数
----@function [parent=#ext.ui.Slider] ctor
----@param #number direction 方向，不能为空
----@param #string background 背景图片名称，加#号，在frameCache中取
----@param #string thumb 背景图片名称，加#号，在frameCache中取
----@param #number min=1 最小值
----@param #number max=100 最大值
----@param #number step=1 步数
--------------------------------------------------------
function Slider:ctor(direction,background,thumb,min,max,step)
    Slider.super.ctor(self)

    assert(direction,"direction不能为nil")
    assert(background,"background不能为nil")
    assert(thumb,"thumb不能为nil")

    self.direction = direction

    self._background = type(backgound)=="string" and ext.Image.new(background) or background
    self._thumb = type(thumb)=="string" and ext.Image.new(thumb) or thumb
    self._min = min or 0
    self._max = max or 100
    self._step = step or 1
    self._value = self._min
    self._thumbTouchDown = false
    self._bgBoundingBox = self._background:getCascadeBoundingBox()

    self:addChild(self._background)
    self:addChild(self._thumb)

    if self.direction == Slider.DIRECTION_HORIZONTAL then
        self._thumb:setPositionX(-self._bgBoundingBox.width/2)
    else
        self._thumb:setPositionY(self._bgBoundingBox.height/2)
    end

    self._thumb:setTouchEnabled(true)
    self._thumb:addNodeEventListener(cc.NODE_TOUCH_EVENT , handler(self, self._touchHandler) )
end

function Slider:_touchHandler(event)
    local name,x,y,prevX,prevY = event.name,event.x,event.y,event.prevX,event.prevY
    if name=="began" then
        self._thumbPosX , self._thumbPosY = self._thumb:getPosition()
        self._startX = x 
        self._startY = y
        return true
    elseif name=="moved" then

        local prevValue = self._value
        if self.direction == Slider.DIRECTION_HORIZONTAL then
            local left = -self._bgBoundingBox.width/2
            local right = self._bgBoundingBox.width/2
            local pos = self._thumbPosX+x-self._startX
            if pos<left then
                pos = left
            elseif pos>right then
                pos = right
            end
            self._thumb:setPositionX(pos)
            local rate =(self._bgBoundingBox.width/2+pos)/self._bgBoundingBox.width
            self._value = rate*(self._max-self._min)+self._min
            self._value = math.round(self._value/self._step)*self._step
            --修正滑块位置
            pos = (self._value-self._min)/(self._max-self._min)*self._bgBoundingBox.width-self._bgBoundingBox.width/2
            self._thumb:setPositionX(pos)
            if self._value~=prevValue and self._onChangeFun~=nil then
                self._onChangeFun(self._value)
            end
        else

            local top = self._bgBoundingBox.height/2
            local bottom = -self._bgBoundingBox.height/2
            local pos = self._thumbPosY+y-self._startY
            if pos>top then
                pos = top
            elseif pos<bottom then
                pos = bottom
            end
            local rate =(self._bgBoundingBox.height/2+pos)/self._bgBoundingBox.height
            self._value = rate*(self._max-self._min)+self._min
            self._value = math.round(self._value/self._step)*self._step

            --修正滑块位置
            pos = (self._value-self._min)/(self._max-self._min)*self._bgBoundingBox.height-self._bgBoundingBox.height/2
            self._thumb:setPositionY(pos)
        end
        if self._value~=prevValue and self._onChangeFun~=nil then
            self._onChangeFun(self._value)
        end

    end
end

function Slider:getValue()
    return self._value
end
function Slider:getMin()
    return self._min
end
function Slider:getMax()
    return self._max
end
function Slider:getStep()
    return self._step
end
function Slider:setValue( value )
    local prevValue = self._value
    if self.direction == Slider.DIRECTION_HORIZONTAL then
        local left = -self._bgBoundingBox.width/2
        local right = self._bgBoundingBox.width/2
        local pos = value/self._max*self._bgBoundingBox.width-self._bgBoundingBox.width/2
        if pos<left then
            pos = left
        elseif pos>right then
            pos = right
        end
        self._thumb:setPositionX(pos)
        local rate =(self._bgBoundingBox.width/2+pos)/self._bgBoundingBox.width
        self._value = rate*(self._max-self._min)+self._min
        self._value = math.round(self._value/self._step)*self._step
        --修正滑块位置
        pos = (self._value-self._min)/(self._max-self._min)*self._bgBoundingBox.width-self._bgBoundingBox.width/2
        self._thumb:setPositionX(pos)
        if self._value~=prevValue and self._onChangeFun~=nil then
            self._onChangeFun(self._value)
        end
    else

        local top = self._bgBoundingBox.height/2
        local bottom = -self._bgBoundingBox.height/2
        local pos = value/self._max*self._bgBoundingBox.height-self._bgBoundingBox.height/2
        if pos>top then
            pos = top
        elseif pos<bottom then
            pos = bottom
        end
        local rate =(self._bgBoundingBox.height/2+pos)/self._bgBoundingBox.height
        self._value = rate*(self._max-self._min)+self._min
        self._value = math.round(self._value/self._step)*self._step

        --修正滑块位置
        pos = (self._value-self._min)/(self._max-self._min)*self._bgBoundingBox.height-self._bgBoundingBox.height/2
        self._thumb:setPositionY(pos)
    end
    if self._value~=prevValue and self._onChangeFun~=nil then
        self._onChangeFun(self._value)
    end

end

function Slider:onChange(fun)
    self._onChangeFun = fun
    return self
end


function Slider:dispose()
    self._onChangeFun  = nil
    self._thumb = nil
    self._background = nil
    self._bgBoundingBox = nil

    Slider.super.dispose(self)
end

return Slider