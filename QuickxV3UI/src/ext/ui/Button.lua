--
-- Author: zhouzhanglin
-- Date: 2014-10-09 09:13:19
--
local Button = class("Button",ext.Component)

--按钮的几个状态
Button.NORMAL   = "normal"
Button.PRESSED  = "pressed"
Button.DISABLED = "disabled"

function Button:ctor(normal,pressed,disabled)
    Button.super.ctor(self)

    assert(normal,"normal不能为nil")
    self:setTouchEnabled(true)

    self._normal = type(normal)=="string" and display.newSprite(normal) or normal
    self._pressed = type(pressed)=="string" and display.newSprite(pressed) or pressed
    self._disabled = type(disabled)=="string" and display.newSprite(disabled) or disabled
    self._currentState = Button.NORMAL

    self.enableTouchScale = false --按下的时候是否有缩放效果
    self.scaleZoom = 0.1
    self:addChild(self._normal)
    if self._pressed then
        self:addChild(self._pressed)
        self._pressed:setVisible(false)
    end
    if self._disabled then
        self:addChild(self._disabled)
        self._disabled:setVisible(false)
    end
end


function Button:_touchHandler(event)
    local name,x,y,prevX,prevY = event.name,event.x,event.y,event.prevX,event.prevY
    event.target = self
    if name=="began" then
        if not self._enable then return false end

        if self.touchBound~=nil then
            local localP = self:convertToNodeSpace(cc.p(x,y))
            if not cc.rectContainsPoint(self.touchBound,localP) then
                return false
            end
        end

        self._touchDelta.x = x
        self._touchDelta.y = y
        self._isRealClick = true
        self:showState(ext.Button.PRESSED)
        if self.enableTouchScale then
            self:stopAllActions()
            local action = cc.ScaleTo:create(0.05,1+self.scaleZoom)
            self:runAction(action)
        end
        if self._onPressFun ~= nil then
            self._onPressFun(event)
        end
        return true
    elseif name=="moved" then
        if self._onMoveFun ~= nil then
            self._onMoveFun(event)
        end
        if self._isRealClick==true and cc.pGetDistance(cc.p(x,y),self._touchDelta)>display.height/640*7 then
            self._isRealClick = false
        end
    else
        self:showState(ext.Button.NORMAL)
        if self.enableTouchScale then
            self:stopAllActions()
            local action = cc.ScaleTo:create(0.05,1)
            self:runAction(action)
        end
        if self._onReleaseFun ~= nil then
            self._onReleaseFun(event)
        end
        if self:hitTestPoint(x,y) then
            if self._onClickFun ~=nil then
                self._onClickFun(event)
            end
            local temp = display.width>display.height and display.height or display.width
            if self._onRealClickFun~= nil and self._isRealClick==true and cc.pGetDistance(cc.p(x,y),self._touchDelta)<temp/640*7 then
                self._onRealClickFun(event)
            end
        elseif self._onReleaseOutFun~=nil then
            self._onReleaseOutFun(event)
        end
    end
end


----
----@function [parent=#ext.Button] 显示按钮的几个状态
----@param #string state 按钮状态名称，常量
----
function Button:showState(state)
    self._currentState = state 

    for _, child in ipairs(self:getChildren()) do
        if child==self._normal or child ==self._pressed or child==self._disabled then
            child:setVisible(false)
        end
    end

    if state == ext.Button.NORMAL then
        self._normal:setVisible(true)
    elseif state == ext.Button.PRESSED then
        if self._pressed then
            self._pressed:setVisible(true)
        else
            self._normal:setVisible(true)
        end
    elseif state==ext.Button.DISABLED then
        if self._disabled then
            self._disabled:setVisible(true)
        else
            self._normal:setVisible(true)
        end
    end
    return self
end

function Button:getCurrentState()
    return self._currentState
end

----
----@function [parent=#ext.Button] 设备此按钮状态是否可用，不可用时如果有dsiabled状态，则显示此状态，否则显示normal状态
----@param #bool flag true表示可用，false为不可用
----
function Button:setEnable(flag)
    if self._enable ~= flag then
        self._enable = flag 
        if flag then
            self:showState(ext.Button.NORMAL)
        else
            self:showState(ext.Button.DISABLED)
        end
    end
    return self
end


function Button:dispose()
    Button.super.dispose(self)

    self._normal = nil 
    self._pressed = nil 
    self._disabled = nil
end

return Button