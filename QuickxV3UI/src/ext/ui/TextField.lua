local TextField = class("TextField",cc.ui.UILabel)


--setDimensions
function TextField:ctor(option)
    if option and option.font and string.find(string.lower(self._fontFilePath),".fnt") then
        option.UILabelType = 1
    end
    TextField.super.ctor(self,option)
end

return TextField