--
-- Author: zhouzhanglin
-- Date: 2014-10-09 12:04:48
--
--[[
local btns={}
for i=1, 4 do
local btn = ext.ToggleButton.new(
cc.Sprite:create("ui/checkbox_normal.png"),
cc.Sprite:create("ui/checkbox_selected.png"),
cc.Sprite:create("ui/checkbox_normal_disabled.png"),
cc.Sprite:create("ui/checkbox_selected_disabled.png")
)
btn.enableTouchScale = true
btn:setUIPosition(100*(i-1),0)
btns[#btns+1] = btn
end
local tab = ext.ToggleGroup.new(btns)
tab:onChange(function(selectedBtn) 

end)
tab:setSelectedButton(btns[2])
self:addChild(tab)
--]]

----
----@module ext.ToggleGroup
----
local ToggleGroup = class("ToggleGroup",ext.Component)

----
----@function [parent=#ext.ToggleGroup] 构造函数
----@param #table btns toggleButon数组
----
function ToggleGroup:ctor( btns )
    ToggleGroup.super.ctor(self)
    self.touchEnable = false

    self._group = btns or {} --所有的toggleButon
    self._selected = nil -- 当前选择的button
    for i, var in ipairs(self._group) do
        self:addChild(var)
        if i==1 then
            self._selected = var
        end
    end

    self:_addListeners()

    if self._selected then
        self._selected:setSelected(true)
        self._selected:setEnable(false)
    end
end

function ToggleGroup:_addListeners()
    for i, v in ipairs(self._group) do
        v:onChange(handler(self, self._onButtonChange))
    end
end

function ToggleGroup:addToggleButton( btn )
    self._group[#self._group+1] = btn
    btn:onChange(self._onButtonChange)
    self:addChild(btn)
    return self
end

function ToggleGroup:removeToggleButton( btn )
    for i, v in ipairs(self._group) do
        if v==btn then
            table.remove(self._group,i)
            self:removeChild(v,true)
        end
    end
    return self
end


function ToggleGroup:_onButtonChange(evt)
    local btn = evt.target
    if btn ~= self._selected then
        for _, var in ipairs(self._group) do
            if btn==var  then
                var:setSelected(true)
                self._selected = var
                self._selected:setEnable(false)
            else
                var:setSelected(false)
                var:setEnable(true)
            end
        end
        if self._onChangeFun ~= nil then
            self._onChangeFun(self._selected)
        end
    end
end

function ToggleGroup:onChange(fun)
    self._onChangeFun = fun
end

----
----@function [parent=#ext.ToggleGroup] 设置一个按钮为选中状态
----@param #ext.ToggleButton btn 某一个按钮
----
function ToggleGroup:setSelectedButton( btn )
    for _, var in ipairs(self._group) do
        var:setSelected(false)
        var:setEnable(true)
    end
    btn:setSelected(true)
    self._selected = btn
    self._selected:setEnable(false)
    if self._onChangeFun ~= nil then
        self._onChangeFun(self._selected)
    end
    return self
end


----
----@function [parent=#ext.ToggleGroup] 返回当前选择的按钮
----@return #ext.ToggleButton 返回的按钮
function ToggleGroup:getSelectedButton()
    return self._selected
end


function ToggleGroup:dispose()
    if self._group then
        for i, v in ipairs(self._group) do
            if v.dispose then
                v:dispose()
            end
        end
    end
    self._group = nil
    self._onChangeFun = nil 
    ToggleGroup.super.dispose(self)
end

return ToggleGroup