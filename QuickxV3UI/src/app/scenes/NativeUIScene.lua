local NativeUIScene = class("NativeUIScene",function()
	return display.newScene("NativeUIScene")
end)

function NativeUIScene:ctor()
    
end

function NativeUIScene:onEnter()
    self:testImage()
	self:testButton()
	self:testCheckBoxButton()
	self:testCheckBoxButtonGroup()
end

function NativeUIScene:testCheckBoxButtonGroup()
    local param = {
        on="UI/building_cancle.png",
        on_pressed="UI/building_cancle.png",
        off="UI/building_ok.png",
        off_pressed="UI/building_ok.png"
    }
    
    local group = cc.ui.UICheckBoxButtonGroup.new():addTo(self):align(display.CENTER,1000, 0)
        :setLayoutSize(500,106)
        :onButtonSelectChanged(function(event)
            print("UICheckBoxButtonGroup Change")
        end)
    for i=1, 4 do
        group:addButton( cc.ui.UICheckBoxButton.new(param):align(display.CENTER) )
    end
    group:getButtonAtIndex(1):setButtonSelected(true)
end

function NativeUIScene:testCheckBoxButton()
    local param = {
        on="UI/frame_add.png",
        on_pressed="UI/frame_add.png",
        off="UI/frame_append.png",
        off_pressed="UI/frame_append.png"
    }
    
    cc.ui.UICheckBoxButton.new(param):pos(800,100):addTo(self):setButtonSelected(false)
        :onButtonStateChanged(function(event)
            print("UICheckBoxButton changed is selected? "..(event.target:isButtonSelected() and "true" or "false"))
        end)
end

function NativeUIScene:testButton()
    local param = {
        normal = "UI/frame_add.png",
        pressed="UI/frame_append.png",
        disabled="UI/frame_append.png"
    }
    cc.ui.UIPushButton.new(param):addTo(self):pos(100,100)
        :onButtonClicked(function(event)
            print("UIPushButton onButtonClicked")
--            event.target:setButtonEnabled(false)
        end)
        :onButtonPressed(function(event)
            event.target:scaleTo(0.1,1.1)
        end)
        :onButtonRelease(function(event)
            event.target:scaleTo(0.1,1)
        end)
end


function NativeUIScene:testImage()
    cc.ui.UIImage.new("UI/load_bk.jpg")
        :align(display.CENTER,display.cx,display.cy)
        :addTo(self)
        :setLayoutSize(display.width,display.height)
end


function NativeUIScene:onExit()
	
end

return NativeUIScene