function __G__TRACKBACK__(errorMessage) 
    print("----------------------------------------") 
    print("LUA ERROR: " .. tostring(errorMessage) .. "\n") 
    print(debug.traceback("", 2)) 
    print("----------------------------------------") 
end 

CCLuaLoadChunksFromZIP("launcher.zip")
package.loaded["launcher.launcher"] = nil 
require("launcher.launcher")