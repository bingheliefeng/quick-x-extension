//
//  LuaObjcFun.m
//  update
//
//  Created by zhouzhanglin on 9/29/14.
//
//

#import "LuaObjcFun.h"
#import "AppConfig.h"
#import "cocos2d.h"
#include "CCLuaBridge.h"

using namespace cocos2d;

#define AppStoreURL(appleID)    [NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=%@", appleID]


@implementation LuaObjcFun

+(BOOL)needInitPlatform
{
    return false;
}

+(void)initPlatform:(NSInteger)callBack{
    CCLuaBridge::pushLuaFunctionById(callBack);
    CCLuaBridge::getStack()->pushCCLuaValue(CCLuaValue::stringValue("successed"));
    CCLuaBridge::getStack()->executeFunction(1);
    CCLuaBridge::releaseLuaFunctionById(callBack);
}

+(NSInteger)getAppVersionCode
{
    return  1;
}

+(void)loadNewApp
{
    NSString *title = @"更新版本";
    NSString *message = @"检测到有一个新版本，请先更新。";
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}

+ (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:AppStoreURL(Apple_ID)]];
}

@end
