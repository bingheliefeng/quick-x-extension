-- Author: zhouzhanglin
-- Date: 2014-09-28 21:54:21
--

local currentFolder = "/Users/zhouzhanglin/Documents/bitbucket/quick-x-extension/AutoUpdate/udp/v1.0.1"
local function hex(s)
    s=string.gsub(s,"(.)",function (x) return string.format("%02X",string.byte(x)) end)
return s
end
local function readFile(path)
    local file = io.open(path, "rb")
    if file then
        local content = file:read("*all")
        io.close(file)
        return content
    end
    return nil
end
require "lfs"
local function findindir(path, wefind, dir_table, r_table, intofolder)
    for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." and file ~= ".DS_Store" and file ~= "flist"  then --and file ~= "launcher.zip"
            local f = path.."/"..file
            
            local attr = lfs.attributes (f)
            assert (type(attr) == "table")
            if attr.mode == "directory" and intofolder then
                table.insert(dir_table, f)
                findindir(f, wefind, dir_table, r_table, intofolder)
            else
                table.insert(r_table, {name = f, size = attr.size})
            end  
        end
    end
end

MakeFileList = {}
function MakeFileList:run(path)
    
    local dir_table = {}
    local input_table = {}
    findindir(currentFolder, ".", dir_table, input_table, true)
    local pthlen = string.len(currentFolder)+2
    local buf = "local flist = {\n"
    buf = buf.."\tappVersion = 1,\n"
    buf = buf.."\tversion = \"1.0.1\",\n"
    buf = buf.."\tdirPaths = {\n"
    for i,v in ipairs(dir_table) do
        --print(i,v)
        local fn = string.sub(v,pthlen)
        buf = buf.."\t\t{name = \""..fn.."\"},\n"
    end
    buf = buf.."\t},\n"
    buf = buf.."\tfileInfoList = {\n"
    for i,v in ipairs(input_table) do
        --print(i,v)
        local fn = string.sub(v.name, pthlen)
        buf = buf.."\t\t{name = \""..fn.."\", code = \""
        local data=readFile(v.name)
        local ms = crypto.md5(hex(data or "")) or ""
        buf = buf..ms.."\", size = ".. v.size .."},\n"
    end
    buf = buf.."\t},\n"
    buf = buf.."}\n\n"
    buf = buf.."return flist"
    io.writefile(currentFolder.."/flist", buf)
end
return MakeFileList