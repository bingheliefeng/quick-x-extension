//  AppPlatform.cpp 

#include "AppPlatform.h" 
#include "cocos2d.h" 


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) 
#include "jni/JniHelper.h" 
#include <jni.h> 
#define  GAME_CLASS_NAME "com/quick/extension/InitPlatform" 
#endif 

using namespace cocos2d; 
extern "C" { 
    std::string getAppBaseResourcePath() 
    { 
        #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
        JniMethodInfo t;
        std::string strAppBaseResourcePath;
        if (JniHelper::getStaticMethodInfo(t, GAME_CLASS_NAME, "getAppBaseResourcePath",
                                           "()Ljava/lang/String;")) {
            jstring baseResourcePathJStr = (jstring)t.env->CallStaticObjectMethod(t.classID, t.methodID);
            strAppBaseResourcePath = JniHelper::jstring2string(baseResourcePathJStr);
            t.env->DeleteLocalRef(t.classID);
            t.env->DeleteLocalRef(baseResourcePathJStr);
        }
        return strAppBaseResourcePath;
        #else
        return "";
        #endif
    }
} 

