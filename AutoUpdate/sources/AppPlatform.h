
#ifndef __xxoo__AppPlatform__ 
#define __xxoo__AppPlatform__ 

#include <string> 

extern "C" { 
    std::string getAppBaseResourcePath(); 
} 

#endif /* defined(__xxoo__AppPlatform__) */
