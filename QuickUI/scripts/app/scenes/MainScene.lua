
local MainScene = class("MainScene", ext.BaseScene)

function MainScene:ctor()
    
end

function MainScene:onEnter()

	local UIRoot = ext.Component.new()
	UIRoot:setPosition(0,display.height)
	self:add(UIRoot)

	local layer1 = ext.Component.new()
    layer1:setUIPosition(display.cx,display.cy)
	UIRoot:add(layer1)

    self:addProgressBar(layer1)
    self:createButton(layer1)
    self:addUIImage(layer1)
    self:createTabBar(UIRoot)
    self:createScale3Image(UIRoot)
    self:createScale9Image(UIRoot)
    -- self:createCenterScrollView(UIRoot)
    self:createUIScrollView(UIRoot)
    -- self:createUIPageScrollView(UIRoot)
    self:createSlider(UIRoot)
    self:createInputText(UIRoot)
    self:createTweener(layer1)
    
    --测试alpha
    layer1:setAlpha(0.8)
    UIRoot:setAlpha(0.8)
    
    local function timeout()
        UIRoot:setAlpha(1)
    end
    self:setTimeout(2,timeout)
end



function MainScene:createTweener(tweenObj)
    
    local o = {x=0,y=0}

    local function onStart(target,param)
        print("onStart")
    end
    local function onUpdate()
        tweenObj:setUIPosition(o.x,o.y)
    end 

    ext.Tweener.addTween(o, {
        x=display.width, y=display.height,
        yoyo = true, target = self,
        time=1, delay=1, transition=ext.Tweener.EaseInOutBounce,
        onStart = onStart,
        onStartParams = {start=1},
        onUpdate = onUpdate,
        onComplete=self.onTweenComplete,
        onCompleteParams={o=o}
    })
end

function MainScene:onTweenComplete(params)
--    ext.Tweener.kill(params.o)
end





function MainScene:addProgressBar(container)
    local proTimer = cc.ProgressTimer:create( cc.Sprite:create("ui/proBarfore.png"))
    proTimer:setType(cc.PROGRESS_TIMER_TYPE_BAR)
    proTimer:setReverseDirection(true)
    proTimer:setBarChangeRate(cc.p(1,0))
    proTimer:setMidpoint(cc.p(1,0))

    self.progress = ext.ProgressBar.new( proTimer, "ui/proBarBg.png" )
    self.progress:setUIPosition(0,100)
    self.progress:setPercentage(40)

    container:addChild( self.progress)
end




function MainScene:createButton(container)
    local btn = ext.Button.new("ui/btn_normal.png")
    btn.enableTouchScale = true
    btn:setUIPosition(0,-200)
    container:addChild(btn)
    btn.dispatcher:addEventListener(ext.TouchEvent.CLICK,self.onClickButton,self)
end
function MainScene:onClickButton(evt)
    print("onClick Button")
end



function MainScene:createTabBar( container )
	
    local btns={}
    for i=1, 4 do
        local btn = ext.ToggleButton.new(
            display.newSprite("ui/checkbox_normal.png"),
            display.newSprite("ui/checkbox_selected.png"),
            display.newSprite("ui/checkbox_normal_disabled.png"),
            display.newSprite("ui/checkbox_selected_disabled.png")
        )
        btn.enableTouchScale = true
        btn:setUIPosition(100*(i-1),0)
        btns[#btns+1] = btn
    end
    local tab = ext.TabBar.new(btns)
    tab.dispatcher:addEventListener(ext.Event.CHANGE ,self.onChange,self)
    tab:setSelectedButton(btns[2])
    tab:setUIPosition(50,50)
    container:addChild(tab)
    
end
function MainScene:onChange(evt)
	print("TabBar onChange")
end



function MainScene:createScale3Image( container )
    local img = ext.Scale3Image.new("ui/checkbox_normal.png",40,20,ext.Scale3Image.DIRECTION_HORIZONTAL)
    img:setWidth(display.width/2)
    img.touchEnable = true
    img:setUIPosition(display.cx,display.cy+200)
    container:addChild(img)
    img.dispatcher:addEventListener(ext.TouchEvent.CLICK , self.onScale3ImageClick,self)
end
function MainScene:onScale3ImageClick(event)
    print("click Scale3Image")
end




function MainScene:createScale9Image( container )
    local img = ext.Scale9Image.new("ui/checkbox_normal.png",40,60,40,60)
    img.touchEnable = true
    img:setUIPosition(display.cx,display.cy)
    container:addChild(img)
    img.dispatcher:addEventListener(ext.TouchEvent.CLICK,self.onScale9ImageClick,self)
    
    img:setSize(300,200)
end
function MainScene:onScale9ImageClick(event)
    print("click Scale9Image")
end


function MainScene:addUIImage(container)
    local img = ext.Image.new("dog.png")
    img.touchEnable = true
    img:setUIPosition(250,-200)
    img.dispatcher:addEventListener(ext.TouchEvent.CLICK , self.onImageClick , self )
    img:setScale(0.4,0.75)
    img:showBorder()
    container:addChild(img)
end
function MainScene:onImageClick(event)
    print("Image click")
end






function MainScene:createCenterScrollView(container)
    self.centerScroll = ext.UICenterScrollView.new(cc.size(display.width-250,200),ext.UIPageScrollView.DIRECTION_HORIZONTAL) --DIRECTION_HORIZONTAL,DIRECTION_VERTICAL
    local font = "fonts/HKYuanMini.ttf" --"fonts/font-issue1343.fnt" 
    for var=1, 15 do
        local img = ext.Image.new("ui/btn_normal.png")
        img:setColor(cc.c3b(255,255,0))

        local tf = ext.TextField.new(var,font)
        tf:getLabel():enableOutline(cc.c4b(255,0,0,255),3)
        img:addChild(tf)

        self.centerScroll:addItem(img)
    end
    self.centerScroll:setUIPosition(display.cx,display.height-100)
    container:addChild(self.centerScroll)
    self.centerScroll:showIndex(2,true)
    self.centerScroll:showBorder()
    self.centerScroll.dispatcher:addEventListener(ext.ScrollEvent.SCROLL_OVER,self.onCenterScrollHandler,self)
    self.centerScroll.dispatcher:addEventListener(ext.ScrollEvent.SCROLL_POSITION_CHANGE,self.onCenterScrollHandler,self)
    self.centerScroll.dispatcher:addEventListener(ext.ScrollEvent.TOUCH_MOVED,self.onCenterScrollHandler,self)
    self.centerScroll.dispatcher:addEventListener(ext.Event.SELECTED,self.onCenterScrollHandler,self)

    local prevBtn = ext.Button.new("ui/btn_normal.png")
    prevBtn:setScale(0.5,0.5)
    prevBtn.enableTouchScale = true
    prevBtn:setUIPosition(50,display.height-100)
    container:addChild(prevBtn)
    prevBtn.dispatcher:addEventListener( ext.TouchEvent.CLICK,self.onPrevCenterHandler,self)

    local nextBtn = ext.Button.new("ui/btn_normal.png")
    nextBtn:setScale(0.5,0.5)
    nextBtn.enableTouchScale = true
    nextBtn:setUIPosition(display.width-50,display.height-100)
    container:addChild(nextBtn)
    nextBtn.dispatcher:addEventListener( ext.TouchEvent.CLICK,self.onNextCenterHandler,self)

    local text="page:"..self.centerScroll:getCurrentPage().."/"..self.centerScroll:getTotalPage()
    self.pageTxt = ext.TextField.new(text,font)
    self.pageTxt:setUIPosition(display.cx,display.height-20)
    container:addChild(self.pageTxt)
end
function MainScene:onPrevCenterHandler(evt)
    self.centerScroll:prevPage()
end
function MainScene:onNextCenterHandler(evt)
    self.centerScroll:nextPage()
end
function MainScene:onCenterScrollHandler(evt)
    if evt.type==ext.ScrollEvent.SCROLL_OVER then
        local text="page:"..self.centerScroll:getCurrentPage().."/"..self.centerScroll:getTotalPage()
        self.pageTxt:setText(text)
    elseif evt.type==ext.Event.SELECTED then
        print("CenterScroll selected item") --evt.userData
    end
end








function MainScene:createUIScrollView(container)
    local scroll = ext.UIScrollView.new(cc.size(display.width-100,display.height-100),ext.UIScrollView.DIRECTION_VERTICAL,3) --DIRECTION_HORIZONTAL,DIRECTION_VERTICAL
    local renders={}
    local font = "fonts/HKYuanMini.ttf" --"fonts/font-issue1343.fnt" 
    for var=1, 38 do
        local img = cc.Sprite:create("ui/btn_normal.png")
        img:setColor(cc.c3b(255,255,0))
        local sp = ext.Button.new(img)
        sp.enableTouchScale = true
--        local tf = TextField.new(var,font)
--        sp:addChild(tf)
--        tf:getLabel():enableOutline(cc.c4b(255,0,0,255),2)
    	sp.dispatcher:addEventListener(ext.TouchEvent.REAL_CLICK,self.onClickScrollViewItem,self)
    	table.insert(renders,sp)
    end
    scroll:setRenders(renders)
    scroll:setUIPosition(display.cx,display.cy)
    container:addChild(scroll)
    scroll:showBorder()
end

function MainScene:onClickScrollViewItem( evt )
    print("click scrollView item")
end






function MainScene:createUIPageScrollView(container)
    self.pageScroll = ext.UIPageScrollView.new(cc.size(display.width-250,200),ext.UIPageScrollView.DIRECTION_HORIZONTAL,3) --DIRECTION_HORIZONTAL,DIRECTION_VERTICAL
    local font = "fonts/HKYuanMini.ttf" --"fonts/font-issue1343.fnt" 
    for var=1, 38 do
        local img = ext.Image.new("ui/btn_normal.png")
        img:setColor(cc.c3b(255,255,0))
        
        local tf = ext.TextField.new(var,font)
        tf:getLabel():enableOutline(cc.c4b(255,0,0,255),3)
        img:addChild(tf)
        
        img.dispatcher:addEventListener(ext.TouchEvent.REAL_CLICK,self.onClickPageScrollViewItem,self)
        self.pageScroll:addItem(img)
    end
    self.pageScroll:setUIPosition(display.cx,display.height-100)
    container:addChild(self.pageScroll)
    self.pageScroll:showBorder()
    self.pageScroll.dispatcher:addEventListener(ext.ScrollEvent.SCROLL_OVER,self.onPageScrollHandler,self)
    self.pageScroll.dispatcher:addEventListener(ext.ScrollEvent.SCROLL_POSITION_CHANGE,self.onPageScrollHandler,self)
    self.pageScroll.dispatcher:addEventListener(ext.ScrollEvent.TOUCH_MOVED,self.onPageScrollHandler,self)
    
    local prevBtn = ext.Button.new("ui/btn_normal.png")
    prevBtn:setScale(0.5,0.5)
    prevBtn:setUIPosition(50,display.height-100)
    container:addChild(prevBtn)
    prevBtn.enableTouchScale = true 
    prevBtn.dispatcher:addEventListener( ext.TouchEvent.CLICK,self.onPrevPageHandler,self)
    
    local nextBtn = ext.Button.new("ui/btn_normal.png")
    nextBtn:setScale(0.5,0.5)
    nextBtn.enableTouchScale = true 
    nextBtn:setUIPosition(display.width-50,display.height-100)
    container:addChild(nextBtn)
    nextBtn.dispatcher:addEventListener( ext.TouchEvent.CLICK,self.onNextPageHandler,self)
    
    local text="page:"..self.pageScroll:getCurrentPage().."/"..self.pageScroll:getTotalPage()
    self.pageTxt = ext.TextField.new(text,font)
    self.pageTxt:setUIPosition(display.cx,display.height-20)
    container:addChild(self.pageTxt)
end
function MainScene:onClickPageScrollViewItem(evt)
    print("onClickPageScrollViewItem")
end
function MainScene:onPrevPageHandler(evt)
	self.pageScroll:prevPage()
end
function MainScene:onNextPageHandler(evt)
	self.pageScroll:nextPage()
end
function MainScene:onPageScrollHandler(evt)
    if evt.type==ext.ScrollEvent.SCROLL_OVER then
	   local text="page:"..self.pageScroll:getCurrentPage().."/"..self.pageScroll:getTotalPage()
        self.pageTxt:setText(text)
	end
end




function MainScene:createSlider(container)
    local bg = ext.Scale3Image.new("ui/checkbox_normal.png",40,20,ext.Scale3Image.DIRECTION_VERTICAL)
    bg:setHeight(display.height/2)
    bg.touchEnable = true
    local thumb = ext.Image.new("ui/checkbox_normal.png")
    thumb:setScaleX(1.2)
    thumb:setColor(cc.c3b(255,126,0))
    thumb.touchEnable = true
    local slider=  ext.Slider.new(ext.Slider.DIRECTION_VERTICAL,bg,thumb,1,100,10)
    slider:setUIPosition(200,display.cy)
    container:addChild(slider)
    slider.dispatcher:addEventListener(ext.Event.CHANGE,self.onSliderChange,self)

    self.sliderText = ext.TextField.new("slider value","fonts/HKYuanMini.ttf",60)
    slider:addChild(self.sliderText)
end
function MainScene:onSliderChange(evt)
    self.sliderText:setText("slider value\n"..evt.userdata)
end





function MainScene:createInputText(container)
	local input = ext.TextInput.new(cc.size(300,50),"ui/checkbox_selected_disabled.png")
	input:setPlaceHolder("test:")
    input:setUIPosition(display.width-300,50)
    input:setMaxLength(8)
	container:addChild(input)
end




function MainScene:onExit()
end

return MainScene
