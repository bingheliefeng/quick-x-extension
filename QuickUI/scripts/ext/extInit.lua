--
-- Author: zhouzhanglin
-- Date: 2014-08-04 22:31:48
--
ext = ext or {}

require("ext.evt.evtInit")  --事件
require("ext.ui.uiInit") --ui

ext.Tweener = require("ext.Tweener")
ext.Timer = require("ext.Timer")

ext.BaseLayer = require("ext.cocos2dx.BaseLayer")
ext.BaseScene = require("ext.cocos2dx.BaseScene")