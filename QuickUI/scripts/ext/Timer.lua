----@module ext.Timer 计时器
----抛出的事件有 TimerEvent中的事件
local Timer = class("Timer",require("ext.evt.EventDispatcher"))

-------------------------------------------------
----@function [parent=#ext.Timer] ctor
----@param #number delay 多少时间执行一次
----@param #number repeatCount=0 执行的次数，默认为0，永远执行
---------------------------------------------------
function Timer:ctor(delay,repeatCount)
    Timer.super.ctor(self)
    self._pause = false
    self._currentCount = 0 
    self._repeatCount = repeatCount or 0
    self.delay = delay
end

-------------------------------------------------
----获取当前的次数
----@function [parent=#ext.Timer] getCurrentCount
----@return #number 次数
---------------------------------------------------
function Timer:getCurrentCount()
    return self._currentCount
end

-------------------------------------------------
----获取当前需要执行的总次数
----@function [parent=#ext.Timer] getRepeatCount
----@return #number 次数
---------------------------------------------------
function Timer:getRepeatCount()
    return self._repeatCount
end

-------------------------------------------------
----重置
----@function [parent=#ext.Timer] reset
---------------------------------------------------
function Timer:reset()
    self._currentCount = 0 
end

-------------------------------------------------
----开始执行Timer
----@function [parent=#ext.Timer] start
-------------------------------------------------
function Timer:start()
    self._currentCount = 0 
    local schdu = cc.Director:getInstance():getScheduler()
    self._entryID = schdu:scheduleScriptFunc(handler(self,self._tick),self.delay,false)
end

function Timer:_tick(parameters)
    if not self._pause then
    	if self:hasEventListener(ext.TimerEvent.TIMER) then
    	   self._currentCount = self._currentCount + 1
    	   self:dispatchEvent(ext.TimerEvent.new(ext.TimerEvent.TIMER))
    	   
    	   if self._repeatCount>0 and self._currentCount == self._repeatCount then
                self:dispatchEvent(ext.TimerEvent.new(ext.TimerEvent.TIMER_COMPLETE))
                cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self._entryID)
    	   end
    	end
	end
end


-------------------------------------------------
----停止Timer
----@function [parent=#ext.Timer] stop
-------------------------------------------------
function Timer:stop()
    if self._entryID then
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self._entryID)
    end
end

-------------------------------------------------
----暂停
----@function [parent=#ext.Timer] pause
-------------------------------------------------
function Timer:pause()
	self._paused = true
end

-------------------------------------------------
----恢复
----@function [parent=#ext.Timer] resume
-------------------------------------------------
function Timer:resume()
	self._pause = false
end

function Timer:dispose()
	if self._entryID then
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self._entryID)
	end
	Timer.super.dispose(self)
end

return Timer