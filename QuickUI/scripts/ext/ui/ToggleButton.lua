----@module ext.ui.ToggleButton
----按钮选中状态变化时，抛出Event.CHANGE 事件
local ToggleButton = class("ToggleButton",require("ext.ui.Button"))
ToggleButton.SELECTED = "selected"
ToggleButton.NORMAL_DISABLED = "normalDisabled"
ToggleButton.SELECTED_DISABLED = "selectedDisabled"

--------------------------------------------------------
----@function [parent=#ext.ui.ToggleButton] 构造函数
----@param #toggleButton normal 默认状态 ,不能为nil 
----@param #toggleButton selected 选中状态 ,不能为nil
--------------------------------------------------------
function ToggleButton:ctor(normal,selected,normalDisabled,selectedDisabled)
    ToggleButton.super.ctor(self,normal)
    assert(selected,"selected不能为nil") 
    self._isSelected = false --当前是否选中
    
    self._selected= type(normal)=="string" and display.newSprite(selected) or selected
    self._normalDisabled = type(pressed)=="string" and display.newSprite(normalDisabled) or normalDisabled
    self._selectedDisabled = type(disabled)=="string" and display.newSprite(selectedDisabled) or selectedDisabled
    
    if self._selected then
        self:addChild(self._selected)
        self._selected:setVisible(false)
    end
    if self._normalDisabled then
        self:addChild(self._normalDisabled)
        self._normalDisabled:setVisible(false)
    end
    if self._selectedDisabled then
        self:addChild(self._selectedDisabled)
        self._selectedDisabled:setVisible(false)
    end
end

--------------------------------------------------------
----@function [parent=#ext.ui.ToggleButton] 显示按钮的几个状态
----@param #string state 按钮状态名称，常量
--------------------------------------------------------
function ToggleButton:showState(state)
    self._currentState = state
    if state~=ext.Button.PRESSED then
        for _, child in ipairs(self:getChildren()) do
            child:setVisible(false)
        end
    end
    if state == ext.Button.NORMAL then
        self._normal:setVisible(true)
    elseif state == ToggleButton.SELECTED then
        self._selected:setVisible(true)
    elseif state == ToggleButton.NORMAL_DISABLED then
        if self._normalDisabled then
            self._normalDisabled:setVisible(true)
        else
            self._normal:setVisible(true)
        end
    elseif state == ToggleButton.SELECTED_DISABLED then
        if self._selectedDisabled then
            self._selectedDisabled:setVisible(true)
        else
            self._selected:setVisible(true)
        end
    end
    return self
end

--------------------------------------------------------
----@function [parent=#ext.ui.ToggleButton] 重写onTouch方法
--------------------------------------------------------
function ToggleButton:onTouch(type,touch,event)
    local result = ToggleButton.super.onTouch(self,type,touch,event)
    if self._enable and self:checkTouch(touch,event) then
        if type==ext.TouchEvent.ENDED or type==ext.TouchEvent.CANCELLED then
            self:setSelected( not self._isSelected)
            if self.dispatcher:hasEventListener( ext.Event.CHANGE ) then
                self.dispatcher:dispatchEvent( ext.Event.new(ext.Event.CHANGE),self )
            end
        end
    end
end



--------------------------------------------------------
----@function [parent=#ext.ui.ToggleButton] 重写enable方法
----@param #bool flag true为可用，false为不可用
--------------------------------------------------------
function ToggleButton:setEnable(flag)
    if self._enable ~= flag then
        self._enable = flag 
        if flag then
            if self._isSelected then
                self:showState(ToggleButton.SELECTED)
            else
                self:showState(ext.Button.NORMAL)
            end
        else
            if self._isSelected then
                self:showState(ToggleButton.SELECTED_DISABLED)
            else
                self:showState(ToggleButton.NORMAL_DISABLED)
            end
        end
    end
    return self
end

--------------------------------------------------------
----@function [parent=#ext.ui.ToggleButton] 设置是否选中状态
----@param #bool flag true为选中状态，false为没选中
--------------------------------------------------------
function ToggleButton:setSelected(flag)
    if self._isSelected~= flag then
        self._isSelected = flag
        if flag then
            if self._enable then
                self:showState(ToggleButton.SELECTED)
            else
                self:showState(ToggleButton.SELECTED_DISABLED)
            end
        else
            if self._enable then
                self:showState(ext.Button.NORMAL)
            else
                self:showState(ToggleButton.NORMAL_DISABLED)
            end
        end
    end
    return self
end

--------------------------------------------------------
----@function [parent=#ext.ui.ToggleButton] 当前是否是选择状态
----@return #bool 返回当前是否被选中
--------------------------------------------------------
function ToggleButton:isSelected()
    return self._isSelected
end


function ToggleButton:dispose()
	ToggleButton.super.dispose(self)
	self._selected = nil
    self._normalDisabled = nil
    self._selectedDisabled = nil
end

return ToggleButton