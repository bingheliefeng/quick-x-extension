----@module ext.ui.UICenterScrollView 居中显示的滚动窗口
----抛出的事件有 ScrollEvent.SCROLL_OVER, ScrollEvent.TOUCH_MOVED
----------------------------------------------------------
local UICenterScrollView = class("UICenterScrollView",require("ext.ui.Component"))
UICenterScrollView.DIRECTION_VERTICAL     = 1
UICenterScrollView.DIRECTION_HORIZONTAL   = 2

function UICenterScrollView:ctor(size,direction,renderGap,centerScale)
	UICenterScrollView.super.ctor(self)
	
    self:setContentSize(size.width,size.height)
    self.touchEnable = true
    self.lockDrag = false
    self.speed=0.5
    self.direction = direction or UICenterScrollView.DIRECTION_HORIZONTAL
	self.renderGap = renderGap or 20
    self.centerScale = centerScale or 1.2 --中间对象的缩放值
    self.maxSpeed = 50
    
    self._defaultItemIndex = 0 --显示哪个
    self._beganPos=cc.p(0,0)
    self._containerPos=cc.p(0,0)
    self._delta=cc.p(0,0)
    self._offsetX = 0
    self._offsetY = 0 
    self._touchIsDown = false
    self._clickRender = false --当前是否在点击Render状态
    
    self._container = cc.Node:create()
    
    local clipNode = cc.ClippingNode:create()
    clipNode:setStencil(self:_createCliper())
    clipNode:setContentSize(size.width,size.height)
    clipNode:addChild(self._container)
    --设置到最左边和最上边
    if self.direction==UICenterScrollView.DIRECTION_HORIZONTAL then
        clipNode:setPosition(-size.width/2,0)
        self._container:setPositionX(size.width/2)
        self._endX = size.width/2
        self._endY = 0
    elseif self.direction==UICenterScrollView.DIRECTION_VERTICAL then
        clipNode:setPosition(0,size.height/2)
        self._container:setPositionY(-size.height/2)
        self._endX = 0
        self._endY = size.height/2
    end
    self._clipNode = clipNode
    self:addChild(self._clipNode)
end

----------------------------------------------------------
----重写getBoundingBox
----@function [parent=#ext.ui.UICenterScrollView] getBoundingBox
----@return #cc.rect 范围
------------------------------------------------------------
function UICenterScrollView:getBoundingBox()
    if self:getContentSize().width>0 and self:getContentSize().height>0 then
        local box = cc.rect(0,0,0,0)
        box.width = self:getContentSize().width*self:getScaleX()
        box.height = self:getContentSize().height*self:getScaleY()
        box.x = -box.width/2+self:getPositionX()
        box.y = -box.height/2+self:getPositionY()
        return box
    end
    return self:_getChildBoundingBox()
end

function UICenterScrollView:_createCliper()
    local cliper = cc.DrawNode:create()
    local size = cc.rect(0,0,self:getContentSize().width,self:getContentSize().height)
    cliper:drawPolygon(
        {cc.p(size.x,size.y),cc.p(size.x+size.width,size.y),cc.p(size.x+size.width,size.y+size.height),cc.p(size.x,size.y+size.height)},
        4,
        cc.c4b(1,1,1,1),
        0,
        cc.c4b(1,1,1,1)
    )
    if self.direction==UICenterScrollView.DIRECTION_HORIZONTAL then
        cliper:setPosition(0,-self:getContentSize().height/2)
    elseif self.direction==UICenterScrollView.DIRECTION_VERTICAL then
        cliper:setPosition(-self:getContentSize().width/2,-self:getContentSize().height)
    end
    return cliper
end

----------------------------------------------------------
----添加render
----@function [parent=#ext.ui.UICenterScrollView] addItem
----@param #Component render 添加的render
----------------------------------------------------------
function UICenterScrollView:addItem(render)
    render.touchEnable = true
    render.stopTouchEvent = false
    
    local renderRect = render:getBoundingBox()
    self._renderSize = renderRect
    if self.direction==UICenterScrollView.DIRECTION_HORIZONTAL then
        local px = self._container:getChildrenCount() * (renderRect.width + self.renderGap)
        render:setPositionX(px)
        self._container:setContentSize(px+renderRect.width,renderRect.height)
    elseif self.direction==UICenterScrollView.DIRECTION_VERTICAL then
        local py = -self._container:getChildrenCount() * (renderRect.height + self.renderGap)
        render:setPositionY(py)
        self._container:setContentSize(renderRect.width,py+renderRect.height)
    end
    self._container:addChild(render)

    render.dispatcher:addEventListener(ext.TouchEvent.REAL_CLICK , self._onClickRenderHandler,self)
end

function UICenterScrollView:_onClickRenderHandler(evt)
    
    local targetIndex = 0
    for index, children in ipairs(self._container:getChildren()) do
        if evt.target==children then
            targetIndex = index
            if self._defaultItemIndex+1 == targetIndex and self.dispatcher:hasEventListener(ext.Event.SELECTED) then
                local event = ext.Event.new( ext.Event.SELECTED)
                event.userdata = evt.target
                self.dispatcher:dispatchEvent(event,self)
            break 
            end
        end
    end
    if targetIndex> 0 and targetIndex~=self._defaultItemIndex+1 then
        self._clickRender = true
        self:showIndex(targetIndex,true)
	end
end

----------------------------------------------------------
----在addItem完成，并且添加到显示列表后调用
----@function [parent=#ext.ui.UICenterScrollView] showIndex
----@param #number index=1 显示的索引值，从1开始
----@param #bool animation=false 是否启动动画
----------------------------------------------------------
function UICenterScrollView:showIndex( index , animation )
    index = index or 1
    animation = animation~=nil and animation or false
    self._defaultItemIndex = index-1
    if self._defaultItemIndex<0 then
        self._defaultItemIndex = 0
    elseif self._defaultItemIndex>self._container:getChildrenCount()-1 then
        self._defaultItemIndex = self._container:getChildrenCount()-1
    end
   
    if self.direction==UICenterScrollView.DIRECTION_VERTICAL then
        self._endY = self._defaultItemIndex*(self._renderSize.height+self.renderGap)-self:getContentSize().height/2
        if not animation then
            self._container:setPositionY( self._endY )
        else
            if not self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame) then
                self.dispatcher:addEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame,self)
            end
        end
        self:_scaleYRenders()
    else
        self._endX = -self._defaultItemIndex*(self._renderSize.width+self.renderGap)+self:getContentSize().width/2

        if not animation then
            self._container:setPositionX( self._endX )
        else
            if not self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame) then
                self.dispatcher:addEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame,self)
            end
        end
        self:_scaleXRenders()
    end
end

----------------------------------------------------------
----滚动到指定的render
----@function [parent=#ext.ui.UICenterScrollView] scrollToRender
----@param #Component render item render
----@param #bool animation=false 是否启用缓动
------------------------------------------------------------
function UICenterScrollView:scrollToRender(render,animation)
    animation =  animation~=nil and animation or false
	local index = 0
    for i, children in ipairs(self._container:getChildren()) do
        if evt.target==render then
            index = i 
            break 
        end
    end
    if index>0 then
        self:showIndex(index,animation)
    end
end


function UICenterScrollView:_onEnterFrame(event)
    if self.direction==UICenterScrollView.DIRECTION_VERTICAL then
        local speedY = (self._endY-self._container:getPositionY())*self.speed 
        if not self._touchIsDown and self._endY-self._container:getContentSize().height>-self:getContentSize().height then 
            speedY = speedY * 0.2 
        elseif not self._touchIsDown and self._endY<0 then
            speedY = speedY * 0.2 
        elseif self._clickRender then
            speedY = speedY * 0.2
        end
        if speedY>self.maxSpeed then 
            speedY=self.maxSpeed
        elseif speedY <-self.maxSpeed then 
            speedY = -self.maxSpeed
        end
        self._container:setPositionY( self._container:getPositionY()+speedY )
        
        self:_scaleYRenders()
        
        if math.abs(speedY)<0.1 then
            self._container:setPositionY( self._endY )
            self.dispatcher:removeEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame)
            if self.dispatcher:hasEventListener(ext.ScrollEvent.SCROLL_OVER) then
                self.dispatcher:dispatchEvent( ext.ScrollEvent.new(ext.ScrollEvent.SCROLL_OVER))
            end
            self._clickRender = false
        end

    elseif self.direction==UICenterScrollView.DIRECTION_HORIZONTAL then

        local speedX = (self._endX-self._container:getPositionX())*self.speed 
        if not self._touchIsDown and self._endX>-self._container:getContentSize().width-self:getContentSize().width/2 then 
            speedX = speedX * 0.2
        elseif not self._touchIsDown and self._endX<self._container:getContentSize().width/2 then 
            speedX = speedX * 0.2
        elseif self._clickRender then
            speedX = speedX * 0.2
        end

        if  speedX>self.maxSpeed then 
            speedX= self.maxSpeed 
        elseif speedX <-self.maxSpeed then 
            speedX= -self.maxSpeed 
        end
        self._container:setPositionX( self._container:getPositionX()+speedX )
        
        --缩放
        self:_scaleXRenders()
        
        if math.abs(speedX)<0.1 then
            self._container:setPositionX( self._endX )
            self.dispatcher:removeEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame)
            if self.dispatcher:hasEventListener(ext.ScrollEvent.SCROLL_OVER) then
                self.dispatcher:dispatchEvent( ext.ScrollEvent.new(ext.ScrollEvent.SCROLL_OVER))
            end
            self._clickRender = false
        end
    end
    
end


function UICenterScrollView:_scaleXRenders()
    if self.centerScale>1 then
        for _, render in ipairs(self._container:getChildren()) do
            local posX =  render:getPositionX()+self._container:getPositionX()-self:getContentSize().width/2
            local sc = math.sin( (self._renderSize.width*4-math.abs(posX))/self._renderSize.width/4 )*self.centerScale
            render:setScale( math.max(self.centerScale/2,sc) )
        end
    end
end

function UICenterScrollView:_scaleYRenders()
    if self.centerScale>1 then
        for _, render in ipairs(self._container:getChildren()) do
            local posY =  render:getPositionY()+self._container:getPositionY()+self:getContentSize().height/2
            local sc = math.sin( (self._renderSize.height*4-math.abs(posY))/self._renderSize.height/4)*self.centerScale
            render:setScale( math.max(self.centerScale/2,sc) )
        end
    end
end

function UICenterScrollView:onTouch(type,touch,event)
    if self.lockDrag or self._clickRender then return end
    
    local point = touch:getLocation()
    if type==ext.TouchEvent.BEGAN then

        self._touchIsDown = true
        self._beganPos.x = point.x
        self._beganPos.y = point.y
        self._containerPos.x = self._container:getPositionX()
        self._containerPos.y = self._container:getPositionY()
        self._endX = self._containerPos.x
        self._endY = self._containerPos.y
        self._delta.x = 0
        self._delta.y = 0
        self._offsetX= 0
        self._offsetY =0
        
        self.dispatcher:removeEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame)

    elseif type==ext.TouchEvent.MOVED then
        
        self.touchChildren = false
        self._offsetX = point.x-self._beganPos.x
        self._offsetY = point.y-self._beganPos.y

        self._endX = self._containerPos.x + self._offsetX 
        self._endY = self._containerPos.y + self._offsetY

        self._delta.x = touch:getDelta().x
        self._delta.y = touch:getDelta().y

        if not self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame) then
            self.dispatcher:addEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame,self)
        end
        if self.dispatcher:hasEventListener(ext.ScrollEvent.TOUCH_MOVED) then
            self.dispatcher:dispatchEvent( ext.ScrollEvent.new(ext.ScrollEvent.TOUCH_MOVED))
        end
    elseif type==ext.TouchEvent.ENDED or type==ext.TouchEvent.CANCELLED then

        self._touchIsDown =false 
        self.touchChildren = true

        self._offsetX = self._delta.x*10
        self._offsetY = self._delta.y*10

        if self.direction==UICenterScrollView.DIRECTION_HORIZONTAL then

            if math.abs(self._offsetX)>10 then
                self._endX =  self._endX + self._offsetX
            else self._endX = self._container:getPositionX() end
            --遍历所有的render，判断哪个与end最接近
            local minDistance = math.huge
            local minI = 0
            for i, var in ipairs(self._container:getChildren()) do
                local pos =  var:getPositionX()+self._endX
                local dis = math.abs(pos-self:getContentSize().width/2)
                if dis<minDistance then
                    minDistance=dis 
                    minI = i
                end
            end
            self._endX = -(minI-1)*(self._renderSize.width+self.renderGap)+self:getContentSize().width/2
            self._defaultItemIndex = minI-1
            
            if math.abs(self._endX-self._container:getPositionX())<2 then
                self._container:setPositionX(self._endX)
            elseif not self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame) then
                self.dispatcher:addEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame,self)
            end

        elseif self.direction==UICenterScrollView.DIRECTION_VERTICAL then

            if math.abs(self._offsetY)>10 then
                self._endY =  self._endY + self._offsetY
            else self._endY = self._container:getPositionY() end
            --遍历所有的render，判断哪个与end最接近
            local minDistance = math.huge
            local minI = 0
            for i, var in ipairs(self._container:getChildren()) do
                local pos =  var:getPositionY()+self._endY
                local dis = math.abs(pos+self:getContentSize().height/2)
                if dis<minDistance then
                    minDistance=dis 
                    minI = i
                end
            end
            self._endY = (minI-1)*(self._renderSize.height+self.renderGap)-self:getContentSize().height/2
            self._defaultItemIndex = minI-1
            
            if math.abs(self._endY-self._container:getPositionY())<2 then
                self._container:setPositionY(self._endY)
            elseif not self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame) then
                self.dispatcher:addEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame,self)
            end
        end
    end
    
end


----------------------------------------------------------
----获取当前页，从1开始
----@function [parent=#ext.ui.UICenterScrollView] getCurrentPage
----@return #number 当前的页码
----------------------------------------------------------
function UICenterScrollView:getCurrentPage()
	return self._defaultItemIndex+1
end

----------------------------------------------------------
----获取总共的页码
----@function [parent=#ext.ui.UICenterScrollView] getTotalPage
----@return #number 总共的页码
------------------------------------------------------------
function UICenterScrollView:getTotalPage()
	return self._container:getChildrenCount()
end

----------------------------------------------------------
----是否有下一页
----@function [parent=#ext.ui.UICenterScrollView] hasNextPage
----@return #bool  是否有下一页
----------------------------------------------------------
function UICenterScrollView:hasNextPage()
    return self._defaultItemIndex+1<self._container:getChildrenCount()
end

----------------------------------------------------------
----是否有上一页
----@function [parent=#ext.ui.UICenterScrollView] hasPrevPage
----@return #bool  是否有上一页
----------------------------------------------------------
function UICenterScrollView:hasPrevPage()
    return self._defaultItemIndex>0
end

----------------------------------------------------------
----上一页
----@function [parent=#ext.ui.UICenterScrollView] prevPage
----------------------------------------------------------
function UICenterScrollView:prevPage()
	if self:hasPrevPage() then
        self:showIndex(self._defaultItemIndex,true)
	end
end

----------------------------------------------------------
----下一页
----@function [parent=#ext.ui.UICenterScrollView] nextPage
--------------------------------------------------------------
function UICenterScrollView:nextPage()
    if self:hasNextPage() then
        self:showIndex(self._defaultItemIndex+2,true)
    end
end

----------------------------------------------------------
----获取所有的Item render
----@function [parent=#ext.ui.UICenterScrollView] getRenders
----@return #table 所有的render 数组
----------------------------------------------------------
function UICenterScrollView:getRenders()
    return self._container:getChildren()
end

----------------------------------------------------------
----返回中间的一个render
----@function [parent=#ext.ui.UICenterScrollView] getCenterRender
----@return #Component 返回中间的一个render
----------------------------------------------------------
function UICenterScrollView:getCenterRender()
    return self._container:getChildren()[self._defaultItemIndex+1]
end



function UICenterScrollView:dispose()
    UICenterScrollView.super.dispose(self)
    self._container = nil
    self._clipNode = nil
    self._beganPos = nil
    self._containerPos = nil 
    self._delta = nil 
end



return UICenterScrollView