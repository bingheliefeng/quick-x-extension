----@module ext.ui.Scale9Image 九宫格
local Scale9Image = class("Scale9Image",require("ext.ui.Component"))

---------------------------------------------
----构造函数
----@function [parent=#ext.ui.Scale3Image] 三分格
----@param #string filename 图片路径
----@param #number left 左边的点，默认为宽度的1/3
----@param #number right 右边的点，默认为宽度的2/3
----@param #number top 上边的点，默认为高度的1/3
----@param #number bottom 下边的点，默认为高度的2/3
-----------------------------------------------
function Scale9Image:ctor(filename,left,right,top,bottom)
    Scale9Image.super.ctor(self)
    local filename = assert(filename,"filename 不能为nil")
    
    self._texture =  cc.Director:getInstance():getTextureCache():addImage(filename)
    self._textureSize = self._texture:getContentSize()
    self._left = left or self._textureSize.width*0.4
    self._right = right or self._textureSize.width*0.6
    self._bottom= top or self._textureSize.height*0.4
    self._top = bottom or self._textureSize.height*0.6

    self._width = 0
    self._height = 0
    self._boundingBox = cc.rect(0,0,0,0)
    
    self._batch = cc.SpriteBatchNode:createWithTexture(self._texture,9)
    self:addChild(self._batch)
    
    self:_split(self._left,self._right,self._top,self._bottom)
end

function Scale9Image:_split(left,right,top,bottom)
    if self._texture then
        local rows = {0, top, bottom,self._textureSize.height }
        local cols = {0, left,right,self._textureSize.width}
        local draw = cc.rect(0,0,0,0)
        for i=1,3 do
            for j=1,3 do
                draw.x = cols[i]
                draw.y = rows[j]
                draw.width = cols[i + 1] - cols[i]
                draw.height = rows[j + 1] - rows[j]
    
                local sprite = cc.Sprite:createWithTexture(self._texture,draw)
                self._batch:addChild(sprite)
                sprite:setName( i.."-"..j)
            end
        end
        self:setSize(self._textureSize.width,self._textureSize.height)
    end
end

---------------------------------------------
----设置宽高
----@function [parent=#ext.ui.Scale9Image] setSize
----@param #number width 宽
----@param #number height 高
---------------------------------------------
function Scale9Image:setSize(width,height)
	if self._width~=width or self._height~=height then
        self._width = width<self._textureSize.width and self._textureSize.width or width
        self._height = height<self._textureSize.height and self._textureSize.height or height
        local middle = self._batch:getChildByName("2-2")
        local middleWidth = self._width-self._left-self._textureSize.width+self._right
        local middleHeight = self._height-self._top-self._textureSize.height+self._bottom
        
        --测试
        middle:setScale( middleWidth/middle:getContentSize().width, middleHeight/middle:getContentSize().height)
        
        local left_top = self._batch:getChildByName("1-1")
        left_top:setPosition(-width/2+left_top:getBoundingBox().width/2 ,height/2-left_top:getBoundingBox().height/2 )
        
        local middle_top = self._batch:getChildByName("2-1")
        middle_top:setScaleX( middleWidth/middle_top:getContentSize().width)
        middle_top:setPositionY(left_top:getPositionY())
        
        local right_top = self._batch:getChildByName("3-1")
        right_top:setPosition( width/2-right_top:getBoundingBox().width/2 , left_top:getPositionY() )
        
        local left_middle = self._batch:getChildByName("1-2")
        left_middle:setScaleY( middleHeight/left_middle:getContentSize().height)
        left_middle:setPositionX(left_top:getPositionX())
        
        local right_middle = self._batch:getChildByName("3-2")
        right_middle:setScaleY( middleHeight/right_middle:getContentSize().height)
        right_middle:setPositionX(right_top:getPositionX())
        
        local left_bottom = self._batch:getChildByName("1-3")
        left_bottom:setPosition(left_top:getPositionX() ,-height/2+left_bottom:getBoundingBox().height/2 )
        
        local bottom_middle = self._batch:getChildByName("2-3")
        bottom_middle:setScaleX( middleWidth/bottom_middle:getContentSize().width)
        bottom_middle:setPositionY(left_bottom:getPositionY())
        
        local bottom_right = self._batch:getChildByName("3-3")
        bottom_right:setPosition(right_top:getPositionX() ,left_bottom:getPositionY())
	end
	
	if width<self._width then
	   self:setScaleX(width/self._width)
	else
	   self:setScaleX(1)
	end
	
    if height<self._height then
        self:setScaleY(height/self._height)
    else
        self:setScaleY(1)
    end
    return self
end


---------------------------------------------
----重写设置宽度
----@function [parent=#ext.ui.Scale9Image] setWidth
----@param #number width 宽度
---------------------------------------------
function Scale9Image:setWidth(width)
    self:setSize(width,self._height)
    return self
end


---------------------------------------------
----重写设置高度
----@function [parent=#ext.ui.Scale9Image] setHeight
----@param #number height 高度
---------------------------------------------
function Scale9Image:setHeight(height)
    self:setSize(self._width,height)
    return self
end


---------------------------------------------
----获取当前的宽度
----@function [parent=#ext.ui.Scale9Image] getWidth
----@return #number 宽度
---------------------------------------------
function Scale9Image:getWidth()
    return self._width
end


---------------------------------------------
----获取当前的高度
----@function [parent=#ext.ui.Scale9Image] getHeight
----@return #number 高度
---------------------------------------------
function Scale9Image:getHeight()
    return self._height
end


---------------------------------------------
----重写设置颜色
----@function [parent=#ext.ui.Scale9Image] setColor
----@param #cc.c3b ccColor3 颜色
---------------------------------------------
function Scale9Image:setColor(ccColor3)
    for _, child in ipairs(self._batch:getChildren()) do
        child:setColor(ccColor3)
    end
    return self
end


---------------------------------------------
----重写getBoundingBox
----@function [parent=#ext.ui.Scale9Image] getBoundingBox
-----------------------------------------------
function Scale9Image:getBoundingBox()
    self._boundingBox.width = self._width*self:getScaleX()
    self._boundingBox.height = self._height*self:getScaleY()
    self._boundingBox.x = -self._boundingBox.width/2+self:getPositionX()
    self._boundingBox.y = -self._boundingBox.height/2+self:getPositionY()
    return self._boundingBox
end

function Scale9Image:dispose()
    self._batch = nil
    self._texture = nil
    self._boundingBox = nil
end


return Scale9Image