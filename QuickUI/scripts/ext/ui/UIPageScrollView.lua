----@module ext.ui.UIPageScrollView 分页显示控件
local UIPageScrollView=class("UIPageScrollView",require("ext.ui.Component"))
UIPageScrollView.DIRECTION_VERTICAL     = 1
UIPageScrollView.DIRECTION_HORIZONTAL   = 2

----------------------------------------------------------
----构造函数
----@function [parent=#ext.ui.UIPageScrollView] ctor
----@param #cc.size size 此控件的大小
----@param #number direction=UIPageScrollView.DIRECTION_HORIZONTAL 方向
----@param #number perCount = 1 一页显示几个render 
----@param #number renderGap=20 一页中的render的间隔
----------------------------------------------------------
function UIPageScrollView:ctor( size,direction,perCount,renderGap)
    UIPageScrollView.super.ctor(self)
    self.direction = direction or UIPageScrollView.DIRECTION_HORIZONTAL
    
    self:setContentSize(size.width,size.height)
    self.touchEnable = true
    self.lockDrag = false
    self.speed=0.5
    self.renderGap = renderGap or 20
    
    self._totalPage = 0 
    self._currentPage = 0 
    self._endPos = 0
    
    self._beganPos=cc.p(0,0)
    self._containerPos=cc.p(0,0)
    self._delta=cc.p(0,0)
    self._offsetX = 0
    self._offsetY = 0 
    self._touchIsDown = false 
    self._renderNum = 0 --render的总数量
    self._perCount = perCount or 1 --一页显示几个render
    
    self._container = cc.Node:create()

    local clipNode = cc.ClippingNode:create()
    clipNode:setStencil(self:_createCliper())
    clipNode:setContentSize(size.width,size.height)
    clipNode:addChild(self._container)
    --设置到最左边和最上边
    if self.direction==UIPageScrollView.DIRECTION_HORIZONTAL then
        clipNode:setPosition(-size.width/2,0)
    elseif self.direction==UIPageScrollView.DIRECTION_VERTICAL then
        clipNode:setPosition(0,size.height/2)
    end
    self._clipNode = clipNode
    self:addChild(self._clipNode)

end

function UIPageScrollView:_createCliper()
    local cliper = cc.DrawNode:create()
    local size = cc.rect(0,0,self:getContentSize().width,self:getContentSize().height)
    cliper:drawPolygon(
        {cc.p(size.x,size.y),cc.p(size.x+size.width,size.y),cc.p(size.x+size.width,size.y+size.height),cc.p(size.x,size.y+size.height)},
        4,
        cc.c4b(1,1,1,1),
        0,
        cc.c4b(1,1,1,1)
    )
    if self.direction==UIPageScrollView.DIRECTION_HORIZONTAL then
        cliper:setPosition(0,-self:getContentSize().height/2)
    elseif self.direction==UIPageScrollView.DIRECTION_VERTICAL then
        cliper:setPosition(-self:getContentSize().width/2,-self:getContentSize().height)
    end
    return cliper
end


----------------------------------------------------------
----重写getBoundingBox方法
----@function [parent=#ext.ui.UIPageScrollView] getBoundingBox
----------------------------------------------------------
function UIPageScrollView:getBoundingBox()
    if self:getContentSize().width>0 and self:getContentSize().height>0 then
        local box = cc.rect(0,0,0,0)
        box.width = self:getContentSize().width*self:getScaleX()
        box.height = self:getContentSize().height*self:getScaleY()
        box.x = -box.width/2+self:getPositionX()
        box.y = -box.height/2+self:getPositionY()
        return box
    end
    return self:_getChildBoundingBox()
end

----------------------------------------------------------
----获取总共的页码
----@function [parent=#ext.ui.UIPageScrollView] getTotalPage
----@return #number 总共的页码
------------------------------------------------------------
function UIPageScrollView:getTotalPage()
	return self._totalPage+1
end

----------------------------------------------------------
----获取当前页，从1开始
----@function [parent=#ext.ui.UIPageScrollView] getCurrentPage
----@return #number 当前的页码
----------------------------------------------------------
function UIPageScrollView:getCurrentPage()
	return self._currentPage+1
end

----------------------------------------------------------
----下一页
----@function [parent=#ext.ui.UIPageScrollView] nextPage
--------------------------------------------------------------
function UIPageScrollView:nextPage()
	if self:hasNextPage() then  self._currentPage =self._currentPage +1 end
	local endPos = 0
    if self.direction==UIPageScrollView.DIRECTION_HORIZONTAL then
        endPos = -self._currentPage*self:getContentSize().width
    elseif self.direction==UIPageScrollView.DIRECTION_VERTICAL then
        endPos = self._currentPage*self:getContentSize().height
    end
    if self._endPos~=endPos then
        self._endPos = endPos
        if self.dispatcher:hasEventListener(ext.ScrollEvent.SCROLL_POSITION_CHANGE) then
            self.dispatcher:dispatchEvent( ext.ScrollEvent.new(ext.ScrollEvent.SCROLL_POSITION_CHANGE))
        end
        if not self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame) then
            self.dispatcher:addEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame,self)
        end
    end
end

----------------------------------------------------------
----上一页
----@function [parent=#ext.ui.UIPageScrollView] prevPage
----------------------------------------------------------
function UIPageScrollView:prevPage()
    if self:hasPrevPage() then  self._currentPage =self._currentPage -1 end
    local endPos = 0
    if self.direction==UIPageScrollView.DIRECTION_HORIZONTAL then
        endPos = -self._currentPage*self:getContentSize().width
    elseif self.direction==UIPageScrollView.DIRECTION_VERTICAL then
        endPos = self._currentPage*self:getContentSize().height
    end
    if math.abs(self._endPos- endPos)>1 then
        self._endPos = endPos
        if self.dispatcher:hasEventListener(ext.ScrollEvent.SCROLL_POSITION_CHANGE) then
            self.dispatcher:dispatchEvent( ext.ScrollEvent.new( ext.ScrollEvent.SCROLL_POSITION_CHANGE))
        end
        if not self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame) then
            self.dispatcher:addEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame,self)
        end
    end
end

function UIPageScrollView:_backPage()
    local endPos = 0
    if self.direction==UIPageScrollView.DIRECTION_HORIZONTAL then
        endPos = -self._currentPage*self:getContentSize().width
    elseif self.direction==UIPageScrollView.DIRECTION_VERTICAL then
        endPos = self._currentPage*self:getContentSize().height
    end
    if math.abs(self._endPos- endPos)>1 then
        self._endPos = endPos 
        if self.dispatcher:hasEventListener(ext.ScrollEvent.SCROLL_POSITION_CHANGE) then
            self.dispatcher:dispatchEvent( ext.ScrollEvent.new( ext.ScrollEvent.SCROLL_POSITION_CHANGE))
        end
        if not self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame) then
            self.dispatcher:addEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame,self)
        end
    end
end

----------------------------------------------------------
----是否有下一页
----@function [parent=#ext.ui.UIPageScrollView] hasNextPage
----@return #bool  是否有下一页
----------------------------------------------------------
function UIPageScrollView:hasNextPage()
    return self._currentPage<self._totalPage
end

----------------------------------------------------------
----是否有上一页
----@function [parent=#ext.ui.UIPageScrollView] hasPrevPage
----@return #bool  是否有上一页
----------------------------------------------------------
function UIPageScrollView:hasPrevPage()
    return self._currentPage>0
end



----------------------------------------------------------
----添加item Render
----@function [parent=#ext.ui.UIPageScrollView] addItem
----@param #Component render 添加一个render
----------------------------------------------------------
function UIPageScrollView:addItem(render)

    render.touchEnable = true
    render.stopTouchEvent = false
    
    if self._renderNum>0 and self._renderNum%self._perCount==0 then
        self._totalPage = self._totalPage+1
    end
    local renderRect = render:getBoundingBox()
    
    if self.direction==UIPageScrollView.DIRECTION_HORIZONTAL then
        local pageOffset = self:getContentSize().width*self._totalPage+renderRect.width/2
        local initOffset = (self:getContentSize().width-renderRect.width*self._perCount-self.renderGap*(self._perCount-1) )*0.5 
        local px = pageOffset+initOffset+(self._renderNum%self._perCount)*(renderRect.width + self.renderGap) 
        render:setPositionX( px )
    elseif self.direction==UIPageScrollView.DIRECTION_VERTICAL then
        local pageOffset = self:getContentSize().height*self._totalPage+renderRect.height/2
        local initOffset = (self:getContentSize().height-renderRect.height*self._perCount-self.renderGap*(self._perCount-1) )*0.5 
        local py = -pageOffset-initOffset-(self._renderNum%self._perCount)*(renderRect.height + self.renderGap) 
        render:setPositionY( py )
    end
    
    self._container:addChild(render)
    self._renderNum = self._renderNum +1 
end


----------------------------------------------------------
----清除所有的items
----@function [parent=#ext.ui.UIPageScrollView] clearItems
----------------------------------------------------------
function UIPageScrollView:clearItems()
	self._container:removeAllChildren()
	self._container:setPosition(0,0)
	self._endPos = 0 
	self._currentPage = 0
end



----------------------------------------------------------
----滑动到某一页
----@function [parent=#ext.ui.UIPageScrollView] scrollToPage
----@param #number page 页码，从1开始
----@param #bool animation=false 是否滑动时有动画
----------------------------------------------------------
function UIPageScrollView:scrollToPage(page,animation)
    animation =  animation~=nil and animation or false

	self.page = self.page-1
    if self.page<0 then page=0
    elseif self.page>=self._totalPage then self.page = self._totalPage end
    self._currentPage=self.page 
    if animation then
        if self.direction==UIPageScrollView.DIRECTION_HORIZONTAL then
            self._endPos = -_currentPage*self:getContentSize().width
        elseif self.direction==UIPageScrollView.DIRECTION_VERTICAL then
            self._endPos = _currentPage*self:getContentSize().height
        end
        if self.dispatcher:hasEventListener(ext.ScrollEvent.SCROLL_POSITION_CHANGE) then
            self.dispatcher:dispatchEvent( ext.ScrollEvent.new(ext.ScrollEvent.SCROLL_POSITION_CHANGE))
        end
        if not self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame) then
            self.dispatcher:addEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame,self)
        end
    else
        if self.direction==UIPageScrollView.DIRECTION_HORIZONTAL then
            self._endPos = -self._currentPage*self:getContentSize().width
            self._container:setPositionX(self._endPos)
        elseif self.direction==UIPageScrollView.DIRECTION_VERTICAL then
            self._endPos = self._currentPage*self:getContentSize().height
            self._container:setPositionY(self._endPos)
        end
       
        self.dispatcher:removeEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame,self)
        if self.dispatcher:hasEventListener(ext.ScrollEvent.SCROLL_OVER) then
            self.dispatcher:dispatchEvent( ext.ScrollEvent.new(ext.ScrollEvent.SCROLL_OVER))
        end
    end
end

----------------------------------------------------------
----滚动到render所在的页
----@function [parent=#ext.ui.UIPageScrollView] scrollToRender
----@param #Component render ItemRender
----@param #bool animation=false 是否启用动画
----------------------------------------------------------
function UIPageScrollView:scrollToRender(render,animation)
    local index = 0
    for i, children in ipairs(self._container:getChildren()) do
        if evt.target==render then
            index = i 
            break 
        end
    end
    if index>0 then
        self:scrollToPage( 1+math.floor(index/self._perCount),animation)
    end
end




function UIPageScrollView:onTouch(type,touch,event)
    if self.lockDrag then return end
    local point = touch:getLocation()
    if type==ext.TouchEvent.BEGAN then

        self._touchIsDown = true
        self._beganPos.x = point.x
        self._beganPos.y = point.y
        self._containerPos.x = self._container:getPositionX()
        self._containerPos.y = self._container:getPositionY()
        self._delta.x = 0
        self._delta.y = 0
        self._offsetX= 0
        self._offsetY =0
        self.dispatcher:removeEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame)

    elseif type==ext.TouchEvent.MOVED then

        self.touchChildren = false
        self._offsetX = point.x-self._beganPos.x
        self._offsetY = point.y-self._beganPos.y
        
        if self.direction==UIPageScrollView.DIRECTION_HORIZONTAL then
            self._endPos = self._containerPos.x +  point.x-self._beganPos.x
        elseif self.direction==UIPageScrollView.DIRECTION_VERTICAL then
            self._endPos = self._containerPos.y +  point.y-self._beganPos.y
        end

        self._delta.x = touch:getDelta().x
        self._delta.y = touch:getDelta().y
        
        if not self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame) then
            self.dispatcher:addEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame,self)
        end
        if self.dispatcher:hasEventListener(ext.ScrollEvent.TOUCH_MOVED) then
            self.dispatcher:dispatchEvent( ext.ScrollEvent.new(ext.ScrollEvent.TOUCH_MOVED))
        end
        
    elseif type==ext.TouchEvent.ENDED or type==ext.TouchEvent.CANCELLED then

        self._touchIsDown =false 
        self.touchChildren = true

        --判断翻页
        if self.direction==UIPageScrollView.DIRECTION_HORIZONTAL and math.abs(self._offsetX)>1 then
            if math.abs(self._delta.x)>5 or math.abs(self._offsetX)>self:getContentSize().width/2 then
                if self._offsetX>0 then
                    self:prevPage()
                else
                    self:nextPage()
                end
            else
                self:_backPage()
            end

            if not self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame) then
                self.dispatcher:addEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame,self)
            end
        elseif self.direction==UIPageScrollView.DIRECTION_VERTICAL and math.abs(self._offsetY)>1 then
        
            if math.abs(self._delta.y)>5 or math.abs(self._offsetY)>self:getContentSize().height/2 then
                if self._offsetY<0 then
                    self:prevPage()
                else
                    self:nextPage()                  
                end
            else
                self:_backPage()
            end
            if not self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME, self._onEnterFrame) then
                self.dispatcher:addEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame,self)
            end
        end
    end

end



function UIPageScrollView:_onEnterFrame(event)
    local currentSpeed = self._touchIsDown==true and self.speed*2 or self.speed

    if self.direction==UIPageScrollView.DIRECTION_HORIZONTAL then
        
        if  math.abs(self._container:getPositionX() - self._endPos)>1 then
            self._container:setPositionX( self._container:getPositionX()+(self._endPos-self._container:getPositionX())*currentSpeed*0.2)
        else
            self._container:setPositionX( self._endPos )
            self.dispatcher:removeEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame)
            if not self._touchIsDown then  
                if self.dispatcher:hasEventListener(ext.ScrollEvent.SCROLL_OVER) then
                    self.dispatcher:dispatchEvent(ext.ScrollEvent.new(ext.ScrollEvent.SCROLL_OVER))
                end
            end
        end
        
    elseif self.direction==UIPageScrollView.DIRECTION_VERTICAL then
        
        if  math.abs(self._container:getPositionY() - self._endPos)>1 then
            self._container:setPositionY( self._container:getPositionY()+(self._endPos-self._container:getPositionY())*currentSpeed*0.2)
        else
            self._container:setPositionY( self._endPos )
            self.dispatcher:removeEventListener(ext.Event.ENTER_FRAME , self._onEnterFrame)
            if not self._touchIsDown then  
                if self.dispatcher:hasEventListener(ext.ScrollEvent.SCROLL_OVER) then
                    self.dispatcher:dispatchEvent(ext.ScrollEvent.new(ext.ScrollEvent.SCROLL_OVER))
                end
            end
        end
        
    end
	
end

----------------------------------------------------------
----获取所有的Item render
----@function [parent=#ext.ui.UIPageScrollView] getRenders
----@return #table 所有的render 数组
----------------------------------------------------------
function UIPageScrollView:getRenders()
    return self._container:getChildren()
end


function UIPageScrollView:dispose()
    UIPageScrollView.super.dispose(self)
    self._container = nil
    self._clipNode = nil
    self._beganPos = nil
    self._containerPos = nil 
    self._delta = nil 
end


return UIPageScrollView