--
-- Author: zhouzhanglin
-- Date: 2014-08-04 22:28:24
--

-------------------------------------------------------
----@module ext.ui.Component  UI组件基类
----抛出的事件有，ext.ext.TouchEvent中的事件,ext.ext.KeyboardEvent中的事件
---------------------------------------------------------
local Component = class("Component",function() 
    return display.newNode()
end)


function Component:ctor()
	self.touchEnable = false --当前元素是否执行touch事件
    self.touchChildren = true --子元素是否执行touch事件
    self.stopTouchEvent = true --是否阻止触摸事件
    self.enableMultiTouch = false -- 默认不支持多点触摸
    self._enable = true
    self._contentSize = cc.size(0,0)
    self._touchDelta = cc.p(0,0)
    self._alpha = 1
    
    self.dispatcher = ext.EventDispatcher.new()

    self:_addTouchListeners()
    self:_addKeyboardListeners()
    
    -- self:scheduleUpdateWithPriorityLua( handler(self,self._scheduleUpdate),0)
    
    self:addNodeEventListener(cc.NODE_ENTER_FRAME_EVENT, handler(self,self._scheduleUpdate))
end


-------------------------------------------------------
----私有方法
-------------------------------------------------------
function Component:_scheduleUpdate(dt)
    if self._enable==true and self.dispatcher:hasEventListener(ext.Event.ENTER_FRAME) then
        local evt = ext.Event.new(ext.Event.ENTER_FRAME)
        evt.userdata = dt
        self.dispatcher:dispatchEvent(evt)
    end
end

-------------------------------------------------------
----私有方法
----@function [parent=#ext.ui.Component] 添加touch侦听
-------------------------------------------------------
function Component:_addTouchListeners()
    local function onTouchBegan(touch,event)
        if not self._enable then return false end

        if self.touchEnable and self:_getParentTouchChildren() then
            if (touch:getId()==0 or self.enableMultiTouch==true) and self:checkTouch(touch,event) then
                self._touchDelta.x = touch:getLocation().x
                self._touchDelta.y = touch:getLocation().y
                if self.stopTouchEvent then
                    event:stopPropagation()
                end
                if self.onTouch then
                    self:onTouch(ext.TouchEvent.BEGAN,touch,event)
                end
                if self.dispatcher:hasEventListener(ext.TouchEvent.BEGAN) then
                    self.dispatcher:dispatchEvent(ext.TouchEvent.new(ext.TouchEvent.BEGAN,touch,event),self)
                end
                return true
            end
        end
        return false
    end
    local function onTouchMoved(touch,event)
        if self.onTouch then
            self:onTouch(ext.TouchEvent.MOVED,touch,event)
        end
        if self.dispatcher:hasEventListener(ext.TouchEvent.MOVED) then
            self.dispatcher:dispatchEvent(ext.TouchEvent.new(ext.TouchEvent.MOVED,touch,event),self)
        end
    end
    local function onTouchEnded(touch,event)
        if self.onTouch then
            self:onTouch(ext.TouchEvent.ENDED,touch,event)
        end
        if self.dispatcher:hasEventListener(ext.TouchEvent.ENDED) then
            self.dispatcher:dispatchEvent(ext.TouchEvent.new(ext.TouchEvent.ENDED,touch,event),self)
        end
        --点击
        if self:checkTouch(touch,event) then
            if self.onTouch then
                self:onTouch(ext.TouchEvent.CLICK,touch,event)
            end
            if self.dispatcher:hasEventListener(ext.TouchEvent.CLICK) then
                self.dispatcher:dispatchEvent(ext.TouchEvent.new(ext.TouchEvent.CLICK,touch,event),self)
            end
            if cc.pGetDistance(touch:getLocation(),self._touchDelta)<7 then
                if self.onTouch then
                    self:onTouch(ext.TouchEvent.REAL_CLICK,touch,event)
                end
                if self.dispatcher:hasEventListener(ext.TouchEvent.REAL_CLICK) then
                    self.dispatcher:dispatchEvent(ext.TouchEvent.new(ext.TouchEvent.REAL_CLICK,touch,event),self)
                end
            end
        end
    end
    local function onTouchCancelled(touch,event)
        if self.onTouch then
            self:onTouch(ext.TouchEvent.CANCELLED,touch,event)
        end
        if self.dispatcher:hasEventListener(ext.TouchEvent.CANCELLED) then
            self.dispatcher:dispatchEvent(ext.TouchEvent.new(ext.TouchEvent.CANCELLED,touch,event),self)
        end
    end
    
    
    local listener = cc.EventListenerTouchOneByOne:create()
    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(onTouchMoved,cc.Handler.EVENT_TOUCH_MOVED)
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED)
    listener:registerScriptHandler(onTouchCancelled,cc.Handler.EVENT_TOUCH_CANCELLED)
    self:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener,self)
end



function Component:_addKeyboardListeners()
    local function onKeyReleased(keyCode,event)
        if self.dispatcher:hasEventListener(ext.KeyboardEvent.PRESSED) then
            self.dispatcher:dispatchEvent(ext.KeyboardEvent.new(ext.KeyboardEvent.PRESSED,keyCode,event),self)
        end
    end

    local listener = cc.EventListenerKeyboard:create()
    listener:registerScriptHandler(onKeyReleased, cc.Handler.EVENT_KEYBOARD_RELEASED )

    self:getEventDispatcher():addEventListenerWithSceneGraphPriority(listener, self)
end

-------------------------------------------------------
----@function [parent=#ext.ui.Component] 获取父类是否允许此控件执行touch
-------------------------------------------------------
function Component:_getParentTouchChildren()
    local parent = self:getParent()
    while parent~=nil do
    	if parent.touchChildren==false then
    	   return false
    	end
        parent = parent:getParent()
    end
    return true
end

-------------------------------------------------------
----@function [parent=#ext.ui.Component] 重写getContentSize方法
-------------------------------------------------------
function Component:getContentSize()
    if self._contentSize.width==0 and self._contentSize.height==0 then
        if self:getChildrenCount()>0 then
            local child = self:getChildAt(1)
            local size = child:getContentSize()
            size.width = size.width*child:getScaleX()
            size.height = size.height*child:getScaleY()
            return size
    	end
	end
	return self._contentSize
end

function Component:setContentSize(width,height)
	self._contentSize.width = width
	self._contentSize.height = height
	return self
end

-------------------------------------------------------
----重写getBoundingBox方法
----@function [parent=#ext.ui.Component]  getBoundingBox
---------------------------------------------------------
function Component:getBoundingBox()
    local size= self:getContentSize()
    local wid = size.width
    local het = size.height
    return cc.rect(self:getPositionX()-wid/2,self:getPositionY()-het/2,wid,het)
end

-------------------------------------------------------
----点击判断
----@function [parent=#ext.display] checkTouch
----@param touch
----@param event
-------------------------------------------------------
function Component:checkTouch( touch,event)
    local target = event:getCurrentTarget()
    local locationInNode = target:getParent():convertToNodeSpace(touch:getLocation())
    local s = target:getBoundingBox()
    if s and cc.rectContainsPoint(s, locationInNode) then
        return true
    end
    return false
end


function Component:onTouch(type,touch,event)

end




-------------------------------------------------------
----获取对旬是否为可用状态
----@function [parent=#ext.ui.Component] getEnable
-------------------------------------------------------
function Component:getEnable()
    return self._enable
end

function Component:setEnable(flag)
    self._enable = flag 
    return self
end

-------------------------------------------------------
----根据索引值来获取子元素
----@function [parent=#ui.Component] getChildAt
----@param #number index 索引值，从1开始
-------------------------------------------------------
function Component:getChildAt(index)
    for i, children in ipairs(self:getChildren()) do
    	if i==index then
    	   return children
    	end
    end
    return nil
end

-------------------------------------------------------
----获取child所在的层索引
----@function [parent=#ext.ui.Component]  getChildIndex
----@param #ext.ui.Component child 子元素
-------------------------------------------------------
function Component:getChildIndex( child )
    for i, children in ipairs(self:getChildren()) do
        if children==child then
            return i
        end
    end
    return 0
end



-------------------------------------------------------
----设置透明度
----@function [parent=#ext.ui.Component] setAlpha
----@param #number alpha 0-1的数字
-------------------------------------------------------
function Component:setAlpha(alpha)
    if alpha>1 then alpha = 1 elseif alpha<0 then alpha=0 end
    
    self._alpha = alpha
    self:_setAlpha(self,alpha)
end

--遍历子元素，设置alpha
function Component:_setAlpha(target,alpha)
    local enable = alpha==1 and false or true
    target:setCascadeOpacityEnabled(enable)
    for key, var in ipairs(target:getChildren()) do
        local temp  = alpha
        if var._alpha~= nil then
            temp=var._alpha*alpha
        end
        var:setOpacity(temp*255)
        var:setCascadeOpacityEnabled(enable)
        self:_setAlpha(var,temp)
    end
end

function Component:getAlpha()
    return self:getOpacity()/255
end
-------------------------------------------------------
----设置UI位置
----@function [parent=#ext.ui.Component] setUIPosition
----@param #number x 离屏幕左边的像素
----@param #number y 离屏幕上边的像素
-------------------------------------------------------
function Component:setUIPosition(x,y)
    self:setPosition(x, -y)
end

-------------------------------------------------------
----返回当前UI坐标，返回 x,y
----@function [parent=#ext.ui.Component] getUIPosition
----@return #number x坐标
----@return #number y坐标
---------------------------------------------------------
function Component:getUIPosition()
    return self:getPositionX(),-self:getPositionY()
end
function Component:setUIPositionX(x)
    self:setPositionX(x)
    return self
end
function Component:getUIPositionX()
    return self:getPositionX()
end
function Component:setUIPositionY(y)
    self:setPositionY(-y)
    return self
end
function Component:getUIPositionY()
    return -self:getPositionY()
end
function Component:setWidth(width)
    self:setScaleX( width/self:getContentSize().width)
    return self
end
function Component:setHeight(height)
    self:setScaleY( height/self:getContentSize().height)
    return self
end
function Component:setSize( width,height)
    height = height or width
    self:setScale( width/self:getContentSize().width, height/self:getContentSize().height)
    return self
end
function Component:setScale(sx,sy)
    sy = sy or sx
    for _, children in ipairs(self:getChildren()) do
        children:setScale(sx,sy)
    end
    return self
end
function Component:setScaleX(sx)
    for _, children in ipairs(self:getChildren()) do
        children:setScaleX(sx)
    end
    return self
end
function Component:setScaleY(sy)
    for _, children in ipairs(self:getChildren()) do
        children:setScaleY(sy)
    end
    return self
end
function Component:getScaleX()
    local child = self:getChildAt(1)
    if child then
        return child:getScaleX()
    end
    return 1
end
function Component:getScaleY()
    local child = self:getChildAt(1)
    if child then
        return child:getScaleY()
    end
    return 1
end


-------------------------------------------------------
----重写设置颜色方法
----@function [parent=#ext.ui.Component] setColor
----@param #cc.c3b ccColor3 颜色
-------------------------------------------------------
function Component:setColor(ccColor3)
    for _, child in ipairs(self:getChildren()) do
        child:setColor(ccColor3)
    end
    return self
end


-------------------------------------------------------
----设置定时器
----@function [parent=#ui.Component] setInterval
----@param #number delay 间隔时间
----@param #function func 结束时的回调函数
----@return #number 返回当前定时器id
-------------------------------------------------------
function Component:setTimeout( delay , func , ...)
    if self._timeId then
        self:clearTimeoutId(self._timeoutId)
    end
    self._timeoutId = self:getScheduler():scheduleScriptFunc(function()
        if arg then
            func(unpack(arg))
        else
            func()
        end
        self:clearTimeout(self._timeoutId)
    end,delay,false) 
    return self._timeoutId
end


-------------------------------------------------------
----清除定时器
----@function [parent=#ui.Component] clearTimeout
----@param #number timeoutId 定时器id
-------------------------------------------------------
function Component:clearTimeout( timeoutId )
    self:getScheduler():unscheduleScriptEntry(timeoutId)
end


-------------------------------------------------------
----设置定时器
----@function [parent=#ui.Component] setInterval
----@param #number interval 间隔时间
----@param #function func 结束时的回调函数
----@return #number 返回当前定时器id
-------------------------------------------------------
function Component:setInterval( interval,func,...)
    if self._intevalId then
        self:clearInterval(self._intevalId)
    end
    self._intevalId = self:getScheduler():scheduleScriptFunc(function()
        if arg then
            func(unpack(arg))
        else
            func()
        end
        self:clearInterval(self._intevalId)
    end,interval,false) 
    return self._intevalId
end


-------------------------------------------------------
----清除定时器
----@function [parent=#ui.Component] clearInterval
----@param #number intervalId 定时器id
-------------------------------------------------------
function Component:clearInterval( intervalId )
    self:getScheduler():unscheduleScriptEntry(intervalId)
end

-------------------------------------------------------
----显示boundingBox范围，请在最后调用，用于调试
----@function [parent=#ui.Component] showBorder
-------------------------------------------------------
function Component:showBorder()
    local node = cc.DrawNode:create()
    local size = self:getBoundingBox()
    size.x = size.x-self:getPositionX()
    size.y = size.y-self:getPositionY()
    node:drawPolygon(
        {cc.p(size.x,size.y),cc.p(size.x+size.width,size.y),cc.p(size.x+size.width,size.y+size.height),cc.p(size.x,size.y+size.height)},
        4,
        cc.c4b(1,1,1,0),
        1,
        cc.c4b(1,1,1,1)
    )
    self:addChild(node)
    return self
end

-------------------------------------------------------
----获取UI内容
----@function [parent=#ext.ui.Component] getUIContent
---------------------------------------------------------
function Component:getUIContent()
	for _, child in ipairs(self:getChildren()) do
		if child:getVisible() then
		  return child
		end
	end
	return nil
end


---------------------------------------------------------
----移除所有的侦听
----@function [parent=#ext.ui.Component] removeAllListeners
---------------------------------------------------------
function Component:removeAllListeners()
    self.dispatcher:removeAllListeners()
    return self
end


---------------------------------------------------------
----释放
----@function [parent=#ext.ui.Component] dispose
---------------------------------------------------------
function Component:dispose()
    self:removeNodeEventListenersByEvent(cc.NODE_ENTER_FRAME_EVENT)
    self:unscheduleUpdate()
    self:removeAllListeners()
    self.dispatcher = nil
end

return Component