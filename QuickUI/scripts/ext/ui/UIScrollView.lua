----@module ext.ui.UIScrollView
---- 抛出 ScrollEvent.TOUCH_MOVED 事件
local UIScrollView = class("UIScrollView",require("ext.ui.Component"))

UIScrollView.DIRECTION_BOTH         = 0
UIScrollView.DIRECTION_HORIZONTAL   = 1
UIScrollView.DIRECTION_VERTICAL     = 2

--------------------------------------------------
----构造函数
----@function [parent=#ext.ui.UIScrollView] ctor
----@param #cc.size size 大小
----@param #number direction=UIScrollView.DIRECTION_HORIZONTAL 方向常量
----@param #number count=1 如果是横向滑动，count表示行数，如果是竖直滑动，count表示列数
--------------------------------------------------
function UIScrollView:ctor(size,direction,count)
	UIScrollView.super.ctor(self)
    
    count = count or 1
    
    self:setContentSize(size.width,size.height)
	self.touchEnable = true
	
    self.direction = direction or UIScrollView.DIRECTION_HORIZONTAL
    self.lockDrag = false 
	self.maxSpeed = 50 
    self.speed=0.5
    
    self._renderSize = cc.size(50,50) --render size
    self._row = 5
    self._col=5 
    if self.direction==UIScrollView.DIRECTION_HORIZONTAL then
        self._row = count
    elseif self.direction==UIScrollView.DIRECTION_VERTICAL then
        self._col = count
    end
    self._beganPos=cc.p(0,0)
    self._containerPos=cc.p(0,0)
    self._delta=cc.p(0,0)
    self._offsetX = 0
    self._offsetY = 0 
    self._touchIsDown = false 

    
    self._container = cc.Node:create()
    
    local clipNode = cc.ClippingNode:create()
    clipNode:setStencil(self:_createCliper())
    clipNode:setContentSize(size.width,size.height)
    clipNode:addChild(self._container)
	--设置到最左边和最上边
    if self.direction==UIScrollView.DIRECTION_HORIZONTAL then
        clipNode:setPosition(-size.width/2,0)
    elseif self.direction==UIScrollView.DIRECTION_VERTICAL then
        clipNode:setPosition(0,size.height/2)
    end
    self._clipNode = clipNode
	self:addChild(self._clipNode)
	
    self.dispatcher:addEventListener(ext.Event.ENTER_FRAME,self._onEnterFrame,self)
end



function UIScrollView:_createCliper()
    local cliper = cc.DrawNode:create()
    local size = cc.rect(0,0,self:getContentSize().width,self:getContentSize().height)
    cliper:drawPolygon(
        {cc.p(size.x,size.y),cc.p(size.x+size.width,size.y),cc.p(size.x+size.width,size.y+size.height),cc.p(size.x,size.y+size.height)},
        4,
        cc.c4b(1,1,1,1),
        0,
        cc.c4b(1,1,1,1)
    )
    if self.direction==UIScrollView.DIRECTION_HORIZONTAL then
        cliper:setPosition(0,-self:getContentSize().height/2)
    elseif self.direction==UIScrollView.DIRECTION_VERTICAL then
        cliper:setPosition(-self:getContentSize().width/2,-self:getContentSize().height)
    end
    return cliper
end

--------------------------------------------------
----重写getBoundingBox方法
----@function [parent=#ext.ui.UIScrollView] getBoundingBox
function UIScrollView:getBoundingBox()
    if self:getContentSize().width>0 and self:getContentSize().height>0 then
        local box = cc.rect(0,0,0,0)
        box.width = self:getContentSize().width*self:getScaleX()
        box.height = self:getContentSize().height*self:getScaleY()
        box.x = -box.width/2+self:getPositionX()
        box.y = -box.height/2+self:getPositionY()
        return box
    end
    return self:_getChildBoundingBox()
end

-- 确定最后的位置
function UIScrollView:_modifyEnd()
    if self._container:getChildrenCount()==0 then
        self._endX=0
        self._endY=0
        return
    end
    if self.direction==UIScrollView.DIRECTION_HORIZONTAL then
        self._endY=0
    elseif self.direction==UIScrollView.DIRECTION_VERTICAL then
        self._endX=0
    end
end


--------------------------------------------------
----设置ItemRenders
----@function [parent=#ext.ui.UIScrollView] setRenders
----@param #cc.size renderSize=renders[1]:getContentSize  render的大小
----@param #number rowGap 每行的间隔
----@param #number colGap 每列的间隔
----------------------------------------------------
function UIScrollView:setRenders(renders,renderSize,rowGap,colGap)
    if renders==nil or #renders==0 then
        error("renders不能为空",1)
        return
    end

    renderSize = renderSize or renders[1]:getContentSize()
    if renderSize.width==0 or renderSize.height==0 then
        local box = renders[1]:getBoundingBox()
        renderSize.width = box.width
        renderSize.height = box.height
    end
    rowGap = rowGap or 15
    colGap = colGap or 5

    self._renderSize = renderSize
    local nn = cc.Node:create()
    self._container:removeAllChildren()
    self._container:setPosition(0,0)
	self._endX = 0
	self._endY = 0
	
    if self.direction==UIScrollView.DIRECTION_HORIZONTAL then
        self._col = math.ceil(#renders/self._row)
        local wid = self._col*(renderSize.width+colGap)-colGap+renderSize.width
        self._container:setContentSize( wid , renderSize.height*self._row)
        self:_showRenderByH(renders,rowGap,colGap)
    elseif self.direction==UIScrollView.DIRECTION_VERTICAL then
        self._row = math.ceil(#renders/self._col)
        local het =  self._row*(renderSize.height+rowGap)-rowGap+renderSize.height
        self._container:setContentSize(renderSize.width*self._col , het )
        self:_showRenderByV(renders,rowGap,colGap)
    else
        self._col = math.ceil(#renders/self._row)
        local wid = self._col*(renderSize.width+colGap)-colGap+renderSize.width
        self._row = math.ceil(#renders/self._col)
        local het =  self._row*(renderSize.height+rowGap)-rowGap+renderSize.height
        self._container:setContentSize(wid,het)
    end
end


function UIScrollView:_showRenderByH(renders,rowGap,colGap)
    local tempRow = 0
    local tempCol = 0
    for i, render in ipairs(renders) do
        render.touchEnable = true
        render.stopTouchEvent = false
        render:setPositionX( tempCol*(self._renderSize.width+colGap) + self._renderSize.width/2 )
        render:setPositionY( -tempRow*(self._renderSize.height+rowGap) + (self._row-1)*self._renderSize.height/2 )
    	self._container:addChild(render)
    	tempRow = tempRow+1
    	if tempRow==self._row then
            tempRow = 0;
            tempCol = tempCol+1
        end
    end
end

function UIScrollView:_showRenderByV(renders,rowGap,colGap)
    local tempRow = 0
    local tempCol = 0
    for i, render in ipairs(renders) do
        render.touchEnable = true
        render.stopTouchEvent = false
        render:setPositionX( tempCol*(self._renderSize.width+colGap) - (self._col-1)*self._renderSize.width/2 )
        render:setPositionY( -tempRow*(self._renderSize.height+rowGap) - self._renderSize.height/2 )
        self._container:addChild(render)
        tempCol = tempCol+1
        if tempCol==self._col then
            tempCol = 0
            tempRow = tempRow+1
        end
    end
end


function UIScrollView:_onEnterFrame(event)
    if self.direction~=UIScrollView.DIRECTION_HORIZONTAL then
        
        local speedY = (self._endY-self._container:getPositionY())*self.speed 
        if not self._touchIsDown and self._endY-self._container:getContentSize().height>-self:getContentSize().height then 
            speedY = speedY * 0.1 
        elseif not self._touchIsDown and self._endY<0 then
            speedY = speedY * 0.1 
        end
        if speedY>self.maxSpeed then 
            speedY=self.maxSpeed
        elseif speedY <-self.maxSpeed then 
            speedY = -self.maxSpeed  
        end
        self._container:setPositionY( self._container:getPositionY()+speedY )
        
    elseif self.direction~=UIScrollView.DIRECTION_VERTICAL then
        
        local speedX = (self._endX-self._container:getPositionX())*self.speed

        if not self._touchIsDown and self._endX+self._container:getContentSize().width>self:getContentSize().width then 
            speedX = speedX * 0.1
        elseif not self._touchIsDown and self._endX>0 then 
            speedX = speedX * 0.1
        end
        if  speedX>self.maxSpeed then 
            speedX= self.maxSpeed 
        elseif speedX <-self.maxSpeed then 
            speedX= -self.maxSpeed 
        end
        self._container:setPositionX( self._container:getPositionX()+speedX )
    end
end




function UIScrollView:onTouch(type,touch,event)
    if self.lockDrag then return end
    local point = touch:getLocation()
    if type==ext.TouchEvent.BEGAN then

        self._touchIsDown = true
        self._beganPos.x = point.x
        self._beganPos.y = point.y
        self._containerPos.x = self._container:getPositionX()
        self._containerPos.y = self._container:getPositionY()
        self._endX = self._containerPos.x
        self._endY = self._containerPos.y
        self._delta.x = 0
        self._delta.y = 0
        self._offsetX= 0
        self._offsetY =0
        
    elseif type==ext.TouchEvent.MOVED then
    
        self.touchChildren = false
        self._offsetX = point.x-self._beganPos.x
        self._offsetY = point.y-self._beganPos.y
 
        self._endX = self._containerPos.x + self._offsetX 
        self._endY = self._containerPos.y + self._offsetY
        
        self._delta.x = touch:getDelta().x
        self._delta.y = touch:getDelta().y
        
        if self.dispatcher:hasEventListener(ext.ScrollEvent.TOUCH_MOVED) then
            self.dispatcher:dispatchEvent( ext.ScrollEventvent.new(ext.ScrollEvent.TOUCH_MOVED))
        end
    elseif type==ext.TouchEvent.ENDED or type==ext.TouchEvent.CANCELLED then
    
        self._touchIsDown =false 
        self.touchChildren = true
        
        self._offsetX = self._delta.x*10
        self._offsetY = self._delta.y*10
        
        if self.direction==UIScrollView.DIRECTION_HORIZONTAL then
            
            if math.abs(self._offsetX)>10 then
                self._endX =  self._endX + self._offsetX
            else self._endX = self._container:getPositionX() end
            
        elseif self.direction==UIScrollView.DIRECTION_VERTICAL then
        
            if math.abs(self._offsetY)>10 then
                self._endY =  self._endY + self._offsetY
            else self._endY = self._container:getPositionY() end
            
        elseif cc.pGetDistance(self._containerPos,self._container:getPosition())>10 then 
            self._endX =  self._endX + self._offsetX
            self._endY =  self._endY + self._offsetY
        else
            self._endX = self._container:getPositionX()
            self._endY = self._container:getPositionY()
        end
    end
    
    if type~=ext.TouchEvent.MOVED then
        if self._endX>0 or self._container:getContentSize().width<self:getContentSize().width then 
            self._endX=0 
        elseif self._endX<-self._container:getContentSize().width+self:getContentSize().width+self._renderSize.width then
            self._endX = -self._container:getContentSize().width+self:getContentSize().width+self._renderSize.width
        end
        
        if self._endY<0 or self._container:getContentSize().height<self:getContentSize().height then
            self._endY=0 
        elseif self._endY>self._container:getContentSize().height-self:getContentSize().height-self._renderSize.height then
            self._endY = self._container:getContentSize().height-self:getContentSize().height-self._renderSize.height
        end
    end
end


--------------------------------------------------
----返回所有的renders
----@function [parent=#ext.ui.UIScrollView] getRenders 
----@return #table renders数组
----------------------------------------------------
function UIScrollView:getRenders()
    return self._container:getChildren()
end


function UIScrollView:dispose()
    UIScrollView.super.dispose(self)
	self._container = nil
	self._clipNode = nil
    self._renderSize= nil
	self._beganPos = nil
	self._containerPos = nil 
	self._delta = nil 
end

return UIScrollView