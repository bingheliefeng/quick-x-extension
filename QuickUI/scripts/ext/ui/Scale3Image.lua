----@module ext.ui.Scale3Image
local Scale3Image = class("Scale3Image",require("ext.ui.Component"))
Scale3Image.DIRECTION_HORIZONTAL = 1
Scale3Image.DIRECTION_VERTICAL = 2

---------------------------------------------
----构造函数
----@function [parent=#ext.ui.Scale3Image] 三分格
----@param #string filename 图片路径
----@param #number firstPos 第一个点的坐标值,默认为宽度的1/3
----@param #number len 长度，默认为宽度的1/3
----@param #number direction=Scale3Image.DIRECTION_HORIZONTA 方向
-----------------------------------------------
function Scale3Image:ctor(filename,firstPos,len,direction)
    Scale3Image.super.ctor(self)
    local filename = assert(filename,"filename 不能为nil")
    self._direction = direction or Scale3Image.DIRECTION_HORIZONTA

    self._texture =  cc.Director:getInstance():getTextureCache():addImage(filename)
    self._textureSize = self._texture:getContentSize()
    self._width = self._textureSize.width
    self._height = self._textureSize.height
    self._boundingBox = cc.rect(0,0,self._width,self._height)
    
    self._firstPos = firstPos<=0 and self._width/3 or firstPos
    self._len = len<=0 and self._width/3 or len
    
    self._batch = cc.SpriteBatchNode:createWithTexture(self._texture,3)
    self:addChild(self._batch)
    
    if self._direction == Scale3Image.DIRECTION_HORIZONTAL then
        self:_splitWidth()
    else
        self:_splitHeight()
    end
end

--根据宽度来切图
function Scale3Image:_splitWidth()
    if self._texture then
        local cols = {0, self._firstPos, self._firstPos + self._len,self._width}
        local draw = cc.rect(0,0,0,self._height)
        for i=1,3 do
            draw.x = cols[i]
            draw.width = cols[i + 1] - cols[i]
            
            local sprite = cc.Sprite:createWithTexture(self._texture,draw)
            self._batch:addChild(sprite)
            sprite:setTag(i)
        end
        self:setWidth(self._width)
    end
end

--根据高度来切图
function Scale3Image:_splitHeight()
    if self._texture then
        local rows = {0, self._firstPos, self._firstPos + self._len,self._height }
        local draw = cc.rect(0,0,self._width,0)
        for i=1,3 do
            draw.y = rows[i]
            draw.height = rows[i + 1] - rows[i]

            local sprite = cc.Sprite:createWithTexture(self._texture,draw)
            self._batch:addChild(sprite)
            sprite:setTag(i)
        end
        self:setHeight(self._height)
    end
end

---------------------------------------------
----重写设置宽度
----@function [parent=#ext.ui.Scale3Image] setWidth
----@param #number width 宽度
---------------------------------------------
function Scale3Image:setWidth(width)
    if self._direction==Scale3Image.DIRECTION_HORIZONTAL and self._width~=width and  self._batch:getChildrenCount()==3 then
        self._width = width < self._textureSize.width and self._textureSize.width or width
        local sp1 = self._batch:getChildByTag( 1 )
        local sp2 = self._batch:getChildByTag( 2 )
        local sp3 = self._batch:getChildByTag( 3 )
        local sp1Width = sp1:getBoundingBox().width
        local sp3Width = sp3:getBoundingBox().width
        local sp2Width = self._width-sp1Width-sp3Width
        sp2:setScaleX( sp2Width/sp2:getContentSize().width)
        sp1:setPositionX(-width/2+sp1Width/2)
        sp3:setPositionX(width/2-sp3Width/2)
    end
    if width<self._width then
        self:setScaleX(width/self._width)
    else
        self:setScaleX(1)
    end
    return self
end

---------------------------------------------
----重写设置高度
----@function [parent=#ext.ui.Scale3Image] setHeight
----@param #number height 高度
---------------------------------------------
function Scale3Image:setHeight(height)
    if self._direction==Scale3Image.DIRECTION_VERTICAL and self._height~=height and  self._batch:getChildrenCount()==3 then
        self._height = height < self._textureSize.height and self._textureSize.height or height
        local sp1 = self._batch:getChildByTag( 1 )
        local sp2 = self._batch:getChildByTag( 2 )
        local sp3 = self._batch:getChildByTag( 3 )
        local sp1Height = sp1:getBoundingBox().height
        local sp3Height = sp3:getBoundingBox().height
        local sp2Height = self._height-sp1Height-sp3Height
        sp2:setScaleY( sp2Height/sp2:getContentSize().height)
        sp3:setPositionY(-height/2+sp1Height/2)
        sp1:setPositionY(height/2-sp3Height/2)
    end
    if height<self._height then
        self:setScaleY(height/self._height)
    else
        self:setScaleY(1)
    end
    return self
end

---------------------------------------------
----重写设置颜色
----@function [parent=#ext.ui.Scale3Image] setColor
----@param #cc.c3b ccColor3 颜色
---------------------------------------------
function Scale3Image:setColor(ccColor3)
    for _, child in ipairs(self._batch:getChildren()) do
        child:setColor(ccColor3)
    end
    return self
end

---------------------------------------------
----重写getBoundingBox
----@function [parent=#ext.ui.Scale3Image] getBoundingBox
-----------------------------------------------
function Scale3Image:getBoundingBox()
    self._boundingBox.width = self._width*self:getScaleX()
    self._boundingBox.height = self._height*self:getScaleY()
    self._boundingBox.x = -self._boundingBox.width/2+self:getPositionX()
    self._boundingBox.y = -self._boundingBox.height/2+self:getPositionY()
    return self._boundingBox
end

---------------------------------------------
----获取当前的宽度
----@function [parent=#ext.ui.Scale3Image] getWidth
----@return #number 宽度
---------------------------------------------
function Scale3Image:getWidth()
	return self._width
end

---------------------------------------------
----获取当前高度
----@function [parent=#ext.ui.Scale3Image] getHeight
----@return #number 高度
---------------------------------------------
function Scale3Image:getHeight()
	return self._height
end


function Scale3Image:dispose()
    self._batch = nil
    self._texture = nil
    self._boundingBox = nil
end


return Scale3Image