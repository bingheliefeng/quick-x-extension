--[[

local proTimer = cc.ProgressTimer:create( cc.Sprite:create("ui/proBarfore.png"))
proTimer:setType(cc.PROGRESS_TIMER_TYPE_BAR)
proTimer:setReverseDirection(true)
proTimer:setBarChangeRate(cc.p(1,0))
proTimer:setMidpoint(cc.p(1,0))

self.progress = ProgressBar.new( proTimer, "ui/proBarBg.png" )
self.progress:setUIPosition(0,100)
self.progress:setPercentage(40)

--]]
local ProgressBar = class("ProgressBar",require("ext.ui.Component"))

---------------------------------------------
----@function [parent=#ext.ui.ProgressBar] 构造函数
----@param #cc.ProgressTimer proTimer 进度条
----@param #string/Sprite background 背景图片名称
-----------------------------------------------
function ProgressBar:ctor(proTimer,background)
    ProgressBar.super.ctor(self)
    self._progressTimer=assert(proTimer,"proTimer不能为空")
    
    if type(background)=="string" then
        self._background = display.newSprite(background)
    else
        self._background = background
    end
 
    if self._background then
        self:addChild(self._background)
    end

    self:addChild(self._progressTimer)
end

function ProgressBar:setPercentage(val)
    self._progressTimer:setPercentage(val)
    return self
end

function ProgressBar:getPercentage()
    return self._progressTimer:getPercentage()
end

return ProgressBar