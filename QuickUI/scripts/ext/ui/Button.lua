----@module ext.ui.Button 按钮类
local Button = class("Button",require("ext.ui.Component"))

--按钮的几个状态
Button.NORMAL   = "normal"
Button.PRESSED  = "pressed"
Button.DISABLED = "disabled"

----
----@function [parent=#ext.ui.Button] 构造函数
----@param #cc.Node normal 默认状态
----@param #cc.Node pressed 按下状态
----@param #cc.Node disabled 不可用状态
----
function Button:ctor(normal,pressed,disabled)
    Button.super.ctor(self)
    assert(normal,"normal不能为nil") 
    
    self.touchEnable = true
    
    self._normal= type(normal)=="string" and display.newSprite(normal) or normal
    self._pressed = type(pressed)=="string" and display.newSprite(pressed) or pressed
    self._disabled = type(disabled)=="string" and display.newSprite(disabled) or disabled
    self._currentState = Button.NORMAL
    
    self.enableTouchScale = false --按下的时候是否有缩放效果
    self:addChild(self._normal)
    if self._pressed then
        self:addChild(self._pressed)
        self._pressed:setVisible(false)
    end
    if self._disabled then
        self:addChild(self._disabled)
        self._disabled:setVisible(false)
    end
end


----
----@function [parent=#ext.ui.Button] 触摸事件
----@param #string type 事件类型,类型为ext.TouchEvent的事件类型常量 
----@param #table touch cocos2dx的touch
----@param #table event cocos2dx的event
----
function Button:onTouch(type,touch,event)
    Button.super.onTouch(self,type,touch,event)
    if self._enable then
        if type==ext.TouchEvent.BEGAN then
            self:showState(Button.PRESSED)
            if not self._pressed and self.enableTouchScale then
                self:stopAllActions()
                local action = cc.ScaleTo:create(0.1,1.1)
                self:runAction(action)
            end
        elseif type==ext.TouchEvent.ENDED or type==ext.TouchEvent.CANCELLED then
            self:showState(Button.NORMAL)
            if self.enableTouchScale then
                self:stopAllActions()
                local action = cc.ScaleTo:create(0.1,1)
                self:runAction(action)
            end
        end
    end
end


----
----@function [parent=#ext.ui.Button] 显示按钮的几个状态
----@param #string state 按钮状态名称，常量
----
function Button:showState(state)
    self._currentState = state 
    for _, child in ipairs(self:getChildren()) do
    	child:setVisible(false)
    end
    if state == Button.NORMAL then
        self._normal:setVisible(true)
    elseif state == Button.PRESSED then
        if self._pressed then
            self._pressed:setVisible(true)
        else
            self._normal:setVisible(true)
        end
    elseif state==Button.DISABLED then
        if self._disabled then
            self._disabled:setVisible(true)
        else
            self._normal:setVisible(true)
        end
    end
    return self
end

----
----@function [parent=#ext.ui.Button] 设备此按钮状态是否可用，不可用时如果有dsiabled状态，则显示此状态，否则显示normal状态
----@param #bool flag true表示可用，false为不可用
----
function Button:setEnable(flag)
    if self._enable ~= flag then
        self._enable = flag 
        if flag then
            self:showState(Button.NORMAL)
        else
            self:showState(Button.DISABLED)
        end
    end
    return self
end

function Button:dispose()
    Button.super.dispose(self)
    self.normal = nil 
    self.pressed = nil 
    self._disabled = nil
end

return Button