--[[
local btns={}
for i=1, 4 do
    local btn = ToggleButton.new(
        cc.Sprite:create("ui/checkbox_normal.png"),
        cc.Sprite:create("ui/checkbox_selected.png"),
        cc.Sprite:create("ui/checkbox_normal_disabled.png"),
        cc.Sprite:create("ui/checkbox_selected_disabled.png")
    )
    btn.enableTouchScale = true
    btn:setUIPosition(100*(i-1),0)
    btns[#btns+1] = btn
end
local tab = TabBar.new(btns)
tab.dispatcher:addEventListener(ext.Event.CHANGE ,self.onChange,self)
tab:setSelectedButton(btns[2])
tab:setUIPosition(100,1000)
root:addChild(tab)
--]]

---------------------------------------------------------
----@module ext.ui.TabBar
----按钮选择变化时，抛出ext.Event.CHANGE 事件
---------------------------------------------------------
local TabBar = class("TabBar",require("ext.ui.Component"))

---------------------------------------------------------
----@function [parent=#ext.ui.TabBar] 构造函数
----@param #table btns toggleButon数组
---------------------------------------------------------
function TabBar:ctor( btns )
    TabBar.super.ctor(self)
    self.touchEnable = false
    
    self._group = btns or {} --所有的toggleButon
    self._selected = nil -- 当前选择的button
    for i, var in ipairs(self._group) do
    	self:addChild(var)
    	if i==1 then
    	    self._selected = var
    	end
    end
    self:_addListeners()
    
    if self._selected then
        self._selected:setSelected(true)
        self._selected:setEnable(false)
    end
end

---------------------------------------------------------
----添加一个ToggleButton
----@function [parent=#ext.ui.TabBar] addToggleButton
----@param #ToggleButton btn 添加一个ToggleButton
---------------------------------------------------------
function TabBar:addToggleButton( btn )
    self._group[#self._group+1] = btn
    btn.dispatcher:addEventListener( ext.Event.CHANGE,self._onButtonChange , self )
    self:addChild(btn)
    return self
end

---------------------------------------------------------
----移除一个button
----@function [parent=#ext.ui.TabBar] removeToggleButton
----@param #ToggleButton btn 移除的btn
---------------------------------------------------------
function TabBar:removeToggleButton( btn )
    for i, v in ipairs(self._group) do
    	if v==btn then
            v.dispatcher:removeEventListener( ext.Event.CHANGE , self._onButtonChange )
            table.remove(self._group,i)
            self:removeChild(v,true)
    	end
    end
    return self
end

function TabBar:_addListeners()
    for i, v in ipairs(self._group) do
        v.dispatcher:addEventListener( ext.Event.CHANGE,self._onButtonChange , self )
    end
end

function TabBar:_onButtonChange(evt)
    local btn = evt.target
    if btn ~= self._selected then
        for _, var in ipairs(self._group) do
            if btn==var  then
                var:setSelected(true)
                self._selected = var
                self._selected:setEnable(false)
            else
                var:setSelected(false)
                var:setEnable(true)
            end
        end
        if self.dispatcher:hasEventListener( ext.Event.CHANGE ) then
            self.dispatcher:dispatchEvent( ext.Event.new(ext.Event.CHANGE),self )
        end
    end
end

---------------------------------------------------------
----设置一个按钮为选中状态
----@function [parent=#ext.ui.TabBar] setSelectedButton
----@param #ToggleButton btn 某一个按钮
---------------------------------------------------------
function TabBar:setSelectedButton( btn )
    for _, var in ipairs(self._group) do
        var:setSelected(false)
        var:setEnable(true)
    end
    btn:setSelected(true)
    self._selected = btn
    self._selected:setEnable(false)
    if self.dispatcher:hasEventListener( ext.Event.CHANGE ) then
        self.dispatcher:dispatchEvent( ext.Event.new(ext.Event.CHANGE),self )
    end
    return self
end


---------------------------------------------------------
-----返回当前选择的按钮
----@function [parent=#ext.ui.TabBar]
----@return #ToggleButton 返回的按钮
---------------------------------------------------------
function TabBar:getSelectedButton()
    return self._selected
end


function TabBar:dispose()
    if self._group then
        for i, v in ipairs(self._group) do
            if v.dispose then
                v:dispose()
            end
        end
    end
    self._group = nil
    TabBar.super.dispose(self)
end

return TabBar