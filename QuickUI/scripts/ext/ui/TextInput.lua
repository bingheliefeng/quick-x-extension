local TextInput = class("TextInput",require("ext.ui.Component"))

function TextInput:ctor(size,file)
	TextInput.super.ctor(self)
    
    self:setContentSize(size.width,size.height)
    
    self._box=cc.EditBox:create(size,cc.Scale9Sprite:create())
    self:addChild(self._box)

    if self._box.addEditBoxEventListener then
    	self._box:addEditBoxEventListener(handler(self, self._onBoxHandler))
	end
end

function TextInput:_onBoxHandler( event, editbox )
    print("TextInput:"..event)
	if event == "began" then
        -- 开始输入
    elseif event == "changed" then
        -- 输入框内容发生变化
    elseif event == "ended" then
        -- 输入结束
    elseif event == "return" then
        -- 从输入框返回
    end
end

function TextInput:getEditBox()
	return self._box
end

function TextInput:setInputMode(mode)
    self._box:setInputMode(mode)
end

function TextInput:setInputFlag(flag)
	self._box:setInputFlag(flag)
end

function TextInput:setColor(ccColor3)
	self._box:setFontColor(ccColor3)
end

function TextInput:setPlaceHolder(text)
	self._box:setPlaceHolder(text)
end

function TextInput:setPlaceholderFontColor(ccColor3)
    self._box:setPlaceholderFontColor(ccColor3)
end

function TextInput:setMaxLength(val)
    self._box:setMaxLength(val)
end

function TextInput:setFontSize(val)
	self._box:setFontSize(val)
end

function TextInput:setFontName(name)
	self._box:setFontName(name)
end

function TextInput:setFont(font,int)
	self._box:setFont(font,int)
end

function TextInput:getText()
    return self._box:getText()
end

--cc.KEYBOARD_RETURNTYPE
function TextInput:setReturnType(type)
	self._box:setReturnType(type)
end



function TextInput:dispose()
	TextInput.super.dipose(self)
	self._box = nil
end

return TextInput