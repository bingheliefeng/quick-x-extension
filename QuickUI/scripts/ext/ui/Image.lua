---------------------------------------------
----@module ext.ui.Image  UI图片
---------------------------------------------
local Image = class("Image",require("ext.ui.Component"))

---------------------------------------------
----构造函数
----@function [parent=#ext.ui.Image]  ctor
----@param #string filename 图片名称，如果前面加#,从cacheFrame中找
---------------------------------------------
function Image:ctor(filename)
	Image.super.ctor(self)
    assert(filename,"filename不能为空")
    
    if type(filename)=="string" then
        self._sp = display.newSprite(filename)
    else
        self._sp = filename
    end

    self:addChild(self._sp)
end

---------------------------------------------
----设置显示的图片名称或路径
----@function [parent=#ext.ui.Image] setFilename
----@param #string filename 图片名称，如果前面加#,从cacheFrame中找
---------------------------------------------
function Image:setFilename(filename)
    self:removeChild(self._sp)
    self._sp = display.newSprite(filename)
    self:addChild(self._sp)
    
    local tt = cc.Sprite:create()
end

---------------------------------------------
----设置显示区域
----@function [parent=#ext.ui.Image] setRect
----@param #cc.rect rect 范围
---------------------------------------------
function Image:setRect(rec)
    self._sp:setTextureRect(rect)
end


---------------------------------------------
----返回此Image的SpriteFrame
----@function [parent=#ext.ui.Image] getSpriteFrame
----@return #cc.SpriteFrame 返回SpriteFrame
---------------------------------------------
function Image:getSpriteFrame()
    return self._sp:getSpriteFrame()
end

function Image:dispose()
	Image.super.dispose(self)
	self._sp = nil
end

return Image