local ScrollEvent = class("ScrollEvent",require("ext.evt.Event"))

ScrollEvent.TOUCH_MOVED = "touchMoved" --触摸移动
ScrollEvent.SCROLL_POSITION_CHANGE="scrollPositionChange"
ScrollEvent.SCROLL_OVER = "scrollOver"

return ScrollEvent