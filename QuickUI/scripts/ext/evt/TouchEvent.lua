-----@module ext.ui.TouchEvent  UI组件中的touch事件

local TouchEvent = class("TouchEvent",require("ext.evt.Event"))
TouchEvent.BEGAN = "began"
TouchEvent.MOVED = "moved"
TouchEvent.ENDED = "ended"
TouchEvent.CANCELLED = "cancelled"
TouchEvent.CLICK = "CLICK"
TouchEvent.REAL_CLICK = "REAL_CLICK" --点击移动小于6像素才触发

function TouchEvent:ctor(type,touch,event)
    TouchEvent.super.ctor(self,type)
    
    self.target = event:getCurrentTarget()
    self.touch = touch
    self.event = event
end


return TouchEvent