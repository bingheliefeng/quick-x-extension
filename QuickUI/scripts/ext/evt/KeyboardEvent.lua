----@module ext.evt.KeyboardEvent 键盘事件
local KeyboardEvent = class("KeyboardEvent",require("ext.evt.Event"))
KeyboardEvent.PRESSED = "pressed"

function KeyboardEvent:ctor(type,keyCode,event)
    TouchEvent.super.ctor(self,type)

    self.target = event:getCurrentTarget()
    self.keyCode = keyCode
    self.event = event
end

return KeyboardEvent