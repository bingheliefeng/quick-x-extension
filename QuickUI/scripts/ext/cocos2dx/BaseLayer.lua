local BaseLayer =class("BaseLayer",function()
    return display.newLayer()
end)




-------------------------------------------------------
----设置定时器
----@function [parent=#ui.BaseLayer] setInterval
----@param #number delay 间隔时间
----@param #function func 结束时的回调函数
----@return #number 返回当前定时器id
-------------------------------------------------------
function BaseLayer:setTimeout( delay , func , ...)
    if self._timeId then
        self:clearTimeoutId(self._timeoutId)
    end
    self._timeoutId = self:getScheduler():scheduleScriptFunc(function()
        if arg then
            func(unpack(arg))
        else
            func()
        end
        self:clearTimeout(self._timeoutId)
    end,delay,false) 
    return self._timeoutId
end


-------------------------------------------------------
----清除定时器
----@function [parent=#ui.BaseLayer] clearTimeout
----@param #number timeoutId 定时器id
-------------------------------------------------------
function BaseLayer:clearTimeout( timeoutId )
    self:getScheduler():unscheduleScriptEntry(timeoutId)
end


-------------------------------------------------------
----设置定时器
----@function [parent=#ui.BaseLayer] setInterval
----@param #number interval 间隔时间
----@param #function func 结束时的回调函数
----@return #number 返回当前定时器id
-------------------------------------------------------
function BaseLayer:setInterval( interval,func,...)
    if self._intevalId then
        self:clearInterval(self._intevalId)
    end
    self._intevalId = self:getScheduler():scheduleScriptFunc(function()
        if arg then
            func(unpack(arg))
        else
            func()
        end
        self:clearInterval(self._intevalId)
    end,interval,false) 
    return self._intevalId
end


-------------------------------------------------------
----清除定时器
----@function [parent=#ui.BaseLayer] clearInterval
----@param #number intervalId 定时器id
-------------------------------------------------------
function BaseLayer:clearInterval( intervalId )
    self:getScheduler():unscheduleScriptEntry(intervalId)
end



return BaseLayer