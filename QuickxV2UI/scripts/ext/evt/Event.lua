--
-- Author: zhouzhanglin
-- Date: 2014-07-14 17:00:01
--

local Event = class("Event")


--一些常用的事件类型
Event.COMPLETE ="complete"
Event.INIT ="init"
Event.CHANGE ="change"
Event.OPEN ="open"
Event.CLOSE = "close"


function Event:ctor( eventType)
	assert(type(eventType)=="string","eventType必须为字符串")

	self.type = eventType --事件类型
	self.userData = nil --数据
	
	self.target = nil  --抛出事件的对象
	self._isDefaultPrevented=false--是否阻止默认的事件侦听回调方法
	self._isStopPropagation=false --是否阻止子节点执行此事件
end

function Event:stopPropagation()
	self._isStopPropagation = true 
end

function Event:preventDefault()
	self._isDefaultPrevented = false
end

function Event:clone()
	return Event.new(self.type)
end

return Event