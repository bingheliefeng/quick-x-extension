--
-- Author: zhouzhanglin
-- Date: 2014-10-09 09:31:46
--
local Component = class("Component",function() 
    return cc.Node:create()
end)

function Component:ctor()
    
    self.enableMultiTouch = false -- 默认不支持多点触摸
    
    self._enable = true

    self._touchDelta = cc.p(0,0)
    
    self:addNodeEventListener(cc.NODE_TOUCH_EVENT , handler(self, self._touchHandler) )
end

function Component:_touchHandler(event)
    local name,x,y,prevX,prevY = event.name,event.x,event.y,event.prevX,event.prevY
    event.target = self
    if name=="began" then
        if not self._enable then return false end

        self._touchDelta.x = x
        self._touchDelta.y = y

        if self._onPressFun ~= nil then
            self._onPressFun(event)
        end
        return true
    elseif name=="moved" then
        if self._onMoveFun ~= nil then
            self._onMoveFun(event)
        end
    elseif name=="ended" or name=="cancelled" then
        if self._onReleaseFun ~= nil then
            self._onReleaseFun(event)
        end
        if self:hitTestPoint(x,y) then
            if self._onClickFun ~=nil then
                self._onClickFun(event)
            end
            if self._onRealClickFun~= nil and ccpDistance(cc.p(x,y),self._touchDelta)<display.height/640*10 then
                self._onRealClickFun(event)
            end
        elseif self._onReleaseOutFun~=nil then
            self._onReleaseOutFun(event)
        end
    end
end

function Component:onClick( fun )
    self._onClickFun = fun 
end
function Component:onRealClick( fun )
    self._onRealClickFun = fun 
end
function Component:onRelease( fun )
    self._onReleaseFun = fun 
end
function Component:onReleaseOut( fun )
    self._onReleaseOutFun = fun 
end
function Component:onPress( fun )
    self._onPressFun = fun 
end
function Component:onMove( fun )
    self._onMoveFun = fun 
end
function Component:onBecameVisible( fun )
    self._onBecameVisibleFun = fun
end
function Component:onBecameInVisible( fun )
    self._onBecameInVisibleFun = fun 
end




function Component:getChildAt(index)
    local array = self:getChildren()
    for i = 0 , array:count()-1 do
        if i==index then
           return array:objectAtIndex(i)
        end
    end
    return nil
end


function Component:getChildIndex( child )
    local array = self:getChildren()
    for i = 0 , array:count()-1 do
        if array:objectAtIndex(i)==child then
            return i
        end
    end
    return -1
end



----重写UI位置方法

function Component:setUIPosition(x,y)
    self:setPosition(x, -y)
end
function Component:getUIPosition()
    return cc.p( self:getPositionX(),-self:getPositionY() )
end
function Component:setUIPositionX(x)
    self:setPositionX(x)
    return self
end
function Component:getUIPositionX()
    self:getPositionX()
end
function Component:setUIPositionY(y)
    self:setPositionY(-y)
    return self
end
function Component:getUIPositionY()
    self:getPositionY()
end
function Component:setWidth(width)
    self:setScaleX( width/self:getContentSize().width)
    return self
end
function Component:setHeight(height)
    self:setScaleY( height/self:getContentSize().height)
    return self
end
function Component:setSize( width,height)
    self:setScaleX( width/self:getContentSize().width)
    self:setScaleY(  height/self:getContentSize().height)
    return self
end

--最后调用
function Component:drawBoundingBox(color , parent)
    local cbb = self:getBoundingBox()
    local left, bottom, width, height = cbb.origin.x, cbb.origin.y, cbb.size.width, cbb.size.height
    local points = {
        {left, bottom},
        {left + width, bottom},
        {left + width, bottom + height},
        {left, bottom + height},
        {left, bottom},
    }
    local box = display.newPolygon(points, 1.0)
    box:setLineColor(color)
    parent:addChild(box, 1000)
end


function Component:hitTestPoint(x,y)
    return self:getCascadeBoundingBox():containsPoint(cc.p(x,y))
end


function Component:dispose()
    self._touchDelta = nil
    self._onReleaseOutFun = nil 
    self._onClickFun = nil 
    self._onRealClickFun = nil 
    self._onReleaseFun = nil 
    self._onPressFun = nil 
    self._onMoveFun = nil 
    self._onBecameVisibleFun = nil
    self._onBecameInVisibleFun = nil
end

return Component