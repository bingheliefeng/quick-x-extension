--
-- Author: zhouzhanglin
-- Date: 2014-10-09 09:20:01
--
ext = ext or {}

ext.Component = require("ext.ui.Component")
ext.Button  = require("ext.ui.Button")
ext.ToggleButton  = require("ext.ui.ToggleButton")
ext.ToggleGroup  = require("ext.ui.ToggleGroup")
ext.UIScrollView = require("ext.ui.UIScrollView")
ext.UICenterScrollView = require("ext.ui.UICenterScrollView")
ext.UIPageScrollView = require("ext.ui.UIPageScrollView")
ext.Image= require("ext.ui.Image")
ext.Slider = require("ext.ui.Slider")
ext.MovieClip = require("ext.ui.MovieClip")
ext.RichText = require("ext.ui.RichText")
ext.RichLabel = require("ext.ui.RichLabel")