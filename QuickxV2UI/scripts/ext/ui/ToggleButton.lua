--
-- Author: zhouzhanglin
-- Date: 2014-10-09 11:30:29
----@module ext.ToggleButton
----按钮选中状态变化时，抛出ext.Event.CHANGE 事件
local ToggleButton = class("ToggleButton",ext.Button)
ToggleButton.SELECTED = "selected"
ToggleButton.NORMAL_DISABLED = "normalDisabled"
ToggleButton.SELECTED_DISABLED = "selectedDisabled"

----
----@function [parent=#ext.ToggleButton] 构造函数
----@param #toggleButton normal 默认状态 ,不能为nil 
----@param #toggleButton selected 选中状态 ,不能为nil
----
function ToggleButton:ctor(normal,selected,normalDisabled,selectedDisabled)
    ToggleButton.super.ctor(self,normal)
    assert(selected,"selected不能为nil") 
    self._isSelected = false --当前是否选中
    
    self._selected= type(normal)=="string" and display.newSprite(selected) or selected
    self._normalDisabled = type(pressed)=="string" and display.newSprite(normalDisabled) or normalDisabled
    self._selectedDisabled = type(disabled)=="string" and display.newSprite(selectedDisabled) or selectedDisabled
    
    if self._selected then
        self:addChild(self._selected)
        self._selected:setVisible(false)
    end
    if self._normalDisabled then
        self:addChild(self._normalDisabled)
        self._normalDisabled:setVisible(false)
    end
    if self._selectedDisabled then
        self:addChild(self._selectedDisabled)
        self._selectedDisabled:setVisible(false)
    end
end

----
----@function [parent=#ext.ToggleButton] 显示按钮的几个状态
----@param #string state 按钮状态名称，常量
----
function ToggleButton:showState(state)
    self._currentState = state
    if state~=ext.Button.PRESSED then
        local array = self:getChildren()
	    for i = 0 , array:count()-1 do
	        local child = array:objectAtIndex(i)
	        if child==self._normal or child ==self._pressed or child==self._disabled or
	        	child==self._selected or child ==self._normalDisabled or child==self._selectedDisabled then
	            child:setVisible(false)
	        end
	    end
    end
    if state == ext.Button.NORMAL then
        self._normal:setVisible(true)
    elseif state == ext.ToggleButton.SELECTED then
        self._selected:setVisible(true)
    elseif state == ext.ToggleButton.NORMAL_DISABLED then
        if self._normalDisabled then
            self._normalDisabled:setVisible(true)
        else
            self._normal:setVisible(true)
        end
    elseif state == ext.ToggleButton.SELECTED_DISABLED then
        if self._selectedDisabled then
            self._selectedDisabled:setVisible(true)
        else
            self._selected:setVisible(true)
        end
    end
    return self
end

----
----@function [parent=#ext.ToggleButton] 重写_touchHandler方法
----
function ToggleButton:_touchHandler(event)

	local name,x,y,prevX,prevY = event.name,event.x,event.y,event.prevX,event.prevY
    event.target = self
    if name=="began" then
        if not self._enable then return false end

        self._touchDelta.x = x
        self._touchDelta.y = y

        self:showState(ext.Button.PRESSED)
        if not self._pressed or self.enableTouchScale then
            self:stopAllActions()
            local action = CCScaleTo:create(0.1,1.1)
            self:runAction(action)
        end
        if self._onPressFun ~= nil then
            self._onPressFun(event)
        end
        return true
    elseif name=="moved" then
        if self._onMoveFun ~= nil then
            self._onMoveFun(event)
        end
    elseif name=="ended" or name=="cancelled" then
        self:showState(ext.Button.NORMAL)
        if self:getScaleX()~=1 then
            self:stopAllActions()
            local action = CCScaleTo:create(0.1,1)
            self:runAction(action)
        end
        if self._onReleaseFun ~= nil then
            self._onReleaseFun(event)
        end
        if self:hitTestPoint(x,y) then
            if self._onClickFun ~=nil then
                self._onClickFun(event)
            end
            if self._onRealClickFun~= nil and ccpDistance(cc.p(x,y),self._touchDelta)<display.height/640*10 then
                self._onRealClickFun(event)
            end
            self:setSelected(not self._isSelected)
            if self._onChangeFun~=nil then
                self._onChangeFun(event)
            end
        elseif self._onReleaseOutFun~=nil then
            self._onReleaseOutFun(event)
        end
    end

end

function ToggleButton:onChange( fun )
	self._onChangeFun = fun 
end



----
----@function [parent=#ext.ToggleButton] 重写enable方法
----@param #bool flag true为可用，false为不可用
----
function ToggleButton:setEnable(flag)
    if self._enable ~= flag then
        self._enable = flag 
        if flag then
            if self._isSelected then
                self:showState(ext.ToggleButton.SELECTED)
            else
                self:showState(ext.Button.NORMAL)
            end
        else
            if self._isSelected then
                self:showState(ext.ToggleButton.SELECTED_DISABLED)
            else
                self:showState(ext.ToggleButton.NORMAL_DISABLED)
            end
        end
    end
    return self
end

----
----@function [parent=#ext.ToggleButton] 设置是否选中状态
----@param #bool flag true为选中状态，false为没选中
----
function ToggleButton:setSelected(flag)
    if self._isSelected~= flag then
        self._isSelected = flag
        if flag then
            if self._enable then
                self:showState(ext.ToggleButton.SELECTED)
            else
                self:showState(ext.ToggleButton.SELECTED_DISABLED)
            end
        else
            if self._enable then
                self:showState(ext.Button.NORMAL)
            else
                self:showState(ext.ToggleButton.NORMAL_DISABLED)
            end
        end
    end
    return self
end

----
----@function [parent=#ext.ToggleButton] 当前是否是选择状态
----@return #bool 返回当前是否被选中
----
function ToggleButton:isSelected()
    return self._isSelected
end


function ToggleButton:dispose()
	ToggleButton.super.dispose()
	self._selected = nil
    self._normalDisabled = nil
    self._selectedDisabled = nil
    self._onChangeFun = nil
end

return ToggleButton