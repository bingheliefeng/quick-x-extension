--
-- Author: zhouzhanglin
-- Date: 2014-10-10 11:31:16
--
----@module ext.ui.MovieClip  Action动画
local MovieClip = class("MovieClip",require("ext.ui.Component"))

-----------------------------------------------------
----静态方法 ，从plist中创建cc.Animation  
----@function [parent=#ext.ui.MovieClip] createAnimaionWithPlist
----@param #string plist 
----@param #string img plist相对应的图片
----@param #string formatname 例如 run_%2d.png
----@param #number startIndex 对应formatname的开始数字
----@param #number endIndex 对应formatname的结束数字
----@param #number duration 动画间隔时间
----@param #number runCount=-1 播放次数
-------------------------------------------------------
function MovieClip.createAnimationWithPlist(plist,img,formatname,startIndex,endIndex,duration,runCount)
    runCount = runCount or -1
    display.addSpriteFrames(plist,img)
    local frames = display.newFrames(formatname, startIndex, endIndex)
    local anim = display.newAnimation(frames,duration,runCount)
    return anim
end

-------------------------------------------------------
----静态方法 ，通过矩形区域创建Animation
----@function [parent=#ext.ui.MovieClip] createAnimationWithRect
----@param #string filepath 图片路径
----@param #cc.Point startPosition 起始位置，左上角为原点
----@param #number frameWidth 一帧的宽
----@param #number frameHeight 一帧的高
----@param #number count 有多少帧
----@param #number duration 动画帧间隔时间
----@param #bool isYoyo=false 是否来回播放的动画
----@param #number runCount-1 动画播放次数
-------------------------------------------------------
function MovieClip:createAnimationWithRect(filepath,startPosition,frameWidth,frameHeight,count,duration,isYoyo,runCount)
    isYoyo = isYoyo == nil and false or isYoyo
    runCount = runCount or -1
    
    local textureCache = CCTextureCache:sharedTextureCache()
    local texture = textureCache:textureForKey(filepath)
	if texture==nil then
        texture = textureCache:addImage(filepath)
	end
    assert(texture ,"texture不能为nil")
	local frames={}
	local row = 0
	for i=0, count-1 do
        local rect = cc.rect(i*frameWidth+startPosition.x,row*frameHeight+startPosition.y,frameWidth,frameHeight)
        local frame = cc.SpriteFrame:createWithTexture(texture,rect)
        table.insert(frames,frame)
        if rect.width>texture:getContentSize().width then
            row = row + 1
        end
--        cc.SpriteFrameCache:getInstance():addSpriteFrame(frame,filepath..rect.x..rect.y)
	end
	if isYoyo then
	   for i=#frames-1 , 2 ,-1 do
            table.insert(frames,frames[i])
	   end
	end
    local anim = display.newAnimation(frames,duration,runCount)
    return anim
end



-------------------------------------------------------
----构造函数
----@function [parent=#ext.ui.MovieClip] ctor
-------------------------------------------------------
function MovieClip:ctor()
	MovieClip.super.ctor(self)
	
	self._sprite = cc.Sprite:create()
    self:addChild(self._sprite)
	self._actions={}
	self._currentAnimationName = nil
end

-------------------------------------------------------
----获取当前正在播放的Animation名称
----@function [parent=#ext.ui.MovieClip] getCurrentAnimationName
----@return #string Animation名称
-------------------------------------------------------
function MovieClip:getCurrentAnimationName()
	return self._currentAnimationName
end

-------------------------------------------------------
----添加动画
----@function [parent=#ext.ui.MovieClip] addAnimationAction
----@param #string actionName 动作名称
----@param #cc.Action animationAction 要添加的动作
-------------------------------------------------------
function MovieClip:addAnimationAction(actionName,animationAction)
    assert(actionName,"actionName 不能为空")
    assert(animationAction,"animationAction 不能为空")
    if self._actions[actionName]== animationAction then
        return self
    end
    animationAction:retain()
    self._actions[actionName] = animationAction
    return self
end


-------------------------------------------------------
----删除一个动画
----@function [parent=#ext.ui.MovieClip] removeAnimationAction
----@param #string actionName 动作名称
-------------------------------------------------------
function MovieClip:removeAnimationAction( actionName)
    self._actions[actionName]:release()
	self._actions[actionName] = nil
	return self
end

-------------------------------------------------------
----移除所有动作
----@function [parent=#ext.ui.MovieClip] removeAllAnimation
-------------------------------------------------------
function MovieClip:removeAllAnimation()
    self:stopAnimation()
    for key, var in pairs(self._actions) do
    	var:release()
    end
	self._actions =  {}
    return self
end

function MovieClip:playAnimation( actionName )
    self._sprite:stopAllActions()
    if self._actions[actionName] ~= nil then
	    self._currentAnimationName = actionName
        self._sprite:runAction( self._actions[actionName] )
	elseif DEBUG>0 then
	   print("没有这个动画:"..actionName)
	end
    return self
end


-------------------------------------------------------
----停止播放动作
----@function [parent=#ext.ui.MovieClip] stopAnimation
-------------------------------------------------------
function MovieClip:stopAnimation()
    self._sprite:stopAllActions()
    return self
end

function MovieClip:dispose()
	self:removeAllAnimation()
	self._acion = nil
	self._sprite = nil 
	MovieClip.super.dispose(self)
end

return MovieClip