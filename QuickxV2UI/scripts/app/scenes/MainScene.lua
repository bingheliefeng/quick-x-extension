require("ext.evt.evtInit")
require("ext.ui.uiInit")

local MainScene = class("MainScene", function()
    return display.newScene("MainScene")
end)

function MainScene:ctor()

    --测试事件---------
    self:testEvent()

    --测试图片
    self:testImage()

    --测试按钮
    self:testButton()

    --测试toggleButton
    self:testToggleButton()

    --测试toggle组
    self:testToggleGroup()

    --测试scrollView
    -- self:testUIScrollView()

    self:testUICenterScrollView()

    --分页控件
    self:testUIPageScrollView()

    --测试Slider
    self:testSlider()

    --测试MovieClip
    self:testMovieClip()

    --测试富文本
    self:testRichText()

    --测试富文本
    self:testRichLabel()
end


function MainScene:testRichLabel()
	self:performWithDelay(function()
		
		local str = "[color=f75d85 fontSize=50]hello[/color][image=UI/Info.png][/image][color=333333 fontSize=30]world[/color]"
		local label = ext.RichLabel.new(str,"Arial",45)
		label:setPosition(ccp(10, 510))
		self:add(label)
		label:playFadeInAnim(10)

	end, 1)
end


function MainScene:testRichText()
	local str = "[color=0FFFFF number=998]这是一条测试数据[/color][color=F0FF00]效果还ok吧[/color][image=UI/Info.png]口[/image][color=FF0F00]+物攻[/color][color=F0FF00]*[0.4][/color][image=UI/Info.png]口[/image][color=F0FF00]*[0.4][/color][color=0FFFFF]清除自身所有负面状态，并向敌军上/下半场吞吐雷电,造成[/color][color=F0FF00][360][/color][image=UI/Info.png]口[/image][color=FF0F00]+物攻[/color][color=F0FF00]*[0.4][/color][image=UI/Info.png]口[/image][color=F0FF00]*[0.4][/color]"
	--str:字符串 fontSize:字体大小  rowWidth:行宽 rowSpace:行间距
	local ricLab = ext.RichText.new(str, "Arial", 45, 400, -4)
	ricLab:setPosition(ccp(10, display.height-60))
	self:addChild(ricLab)
	-- 添加事件监听函数
	local function listener(button, params)
	    print(params.text, params.tag, params.number)
	    if params.number == 998 then
	        print("预约事件")
	    end
	end
	ricLab:setClilckEventListener(listener)
end


function MainScene:testMovieClip()
	local mc = ext.MovieClip.new()
	mc:setScale(2)
	mc:setPosition(cc.p(display.width-200,display.height-200))
	self:addChild(mc)

	local leftRunAnim = ext.MovieClip.createAnimationWithRect(self,"UI/character.png", cc.p(0,0), 408/4, 276/2, 4, 0.08, true )
	local leftRunAction = CCRepeatForever:create(CCAnimate:create(leftRunAnim))
	mc:addAnimationAction("leftRun", leftRunAction)

	local rightRunAnim = ext.MovieClip.createAnimationWithRect(self,"UI/character.png", cc.p(0,276/2), 408/4, 276/2, 4, 0.08, true )
	local rightRunAction = CCRepeatForever:create(CCAnimate:create(rightRunAnim))
	mc:addAnimationAction("rightRun", rightRunAction)

	mc:playAnimation("leftRun")
	mc:setTouchEnabled(true)
	mc:onClick(function(event)
		if mc:getCurrentAnimationName()=="leftRun" then
			mc:playAnimation("rightRun")
		else
			mc:playAnimation("leftRun")
		end
	end)
end



function MainScene:testSlider()
	local txt = ui.newTTFLabel({text = "", size = 64, align = ui.TEXT_ALIGN_CENTER,color=cc.c3b(255,0,0)})
		:pos(display.cx,display.height-100)

	local bg = display.newScale9Sprite("UI/email_bk.png", 0, 0, cc.size(800,120))
	bg:setScaleY(0.7)
	local thumb = ext.Button.new("UI/building_cancle.png")
	thumb.enableTouchScale = true

	local slider =  ext.Slider.new(ext.Slider.DIRECTION_HORIZONTAL,bg,thumb,1,100,10)
	slider:onChange(function( value )
		txt:setString(value)
	end)
	slider:setPosition(cc.p(display.cx,display.height-100))
	self:add(slider)
	self:add(txt)

	slider:setValue(60)
end


function MainScene:testUIPageScrollView()
	local page = ui.newTTFLabel({text = "", size = 64, align = ui.TEXT_ALIGN_CENTER,color=cc.c3b(255,0,0)})
		:pos(display.cx,display.cy)
	local scroll = ext.UIPageScrollView.new(cc.size(display.width-250,250),ext.UIPageScrollView.DIRECTION_HORIZONTAL,4) --DIRECTION_HORIZONTAL,DIRECTION_VERTICAL
	for var=1, 18 do
        local sp = ext.Image.new("UI/frame_head_selected.png")
        if var==1 then
			sp:onBecameVisible(function()
	        	print("visible")
	        end)
	        sp:onBecameInVisible(function()
	        	print("Invisible")
	        end)
        end
    	scroll:addItem(sp)
    end
    scroll:onPageChange(function( currentPage , totalPage)
    	page:setString(currentPage.." / "..totalPage)
    end)
    scroll:setPosition(display.cx,display.cy)
    self:addChild(scroll)
    self:addChild(page)
    scroll:scrollToPage(2,true)

	-- scroll:performWithDelay(function()
	-- 	scroll:drawBoundingBox(ccc4f(1,0,0,1) , self)
	-- end, 0.2)
end



function MainScene:testUICenterScrollView( )
	local page = ui.newTTFLabel({text = "", size = 64, align = ui.TEXT_ALIGN_CENTER,color=cc.c3b(255,0,0)})
		:pos(display.cx,display.cy-300)

	local scroll = ext.UICenterScrollView.new(cc.size(display.width-200,300),ext.UICenterScrollView.DIRECTION_HORIZONTAL) --DIRECTION_HORIZONTAL,DIRECTION_VERTICAL
    for var=1, 18 do
        local sp = ext.Image.new("UI/frame_head_selected.png")
    	scroll:addItem(sp)
    end
    scroll:setPosition(display.cx,display.cy-300)
    scroll:onPageChange(function( currentPage , totalPage)
    	page:setString(currentPage.." / "..totalPage)
    end)
    self:addChild(scroll)
    self:addChild(page)
    scroll:showIndex(2,true)
end



function MainScene:testUIScrollView()
    local scroll = ext.UIScrollView.new(cc.size(display.width-200,300),ext.UIScrollView.DIRECTION_HORIZONTAL,1) --DIRECTION_HORIZONTAL,DIRECTION_VERTICAL
    local renders={}
    for var=1, 18 do
        local sp = ext.Button.new("UI/frame_head_selected.png")
        sp.enableTouchScale = true
        sp:onRealClick(function(event)
        	print("click scrollView item")
        end)
    	table.insert(renders,sp)
    end
    scroll:setRenders(renders)
    scroll:setPosition(display.cx,display.cy)
    self:addChild(scroll)
end





function MainScene:testToggleGroup()
	
	local btns={}
	for i=1, 4 do
	    local btn = ext.ToggleButton.new(
	        cc.Sprite:create("UI/building_ok.png"),
	        cc.Sprite:create("UI/building_cancle.png")
	    )
	    btn.enableTouchScale = true
	    btn:setUIPosition(100*(i-1),0)
	    btns[#btns+1] = btn
	end
	local tab = ext.ToggleGroup.new(btns)
	tab:onChange(function(selectedBtn) 
		print("toggleGroup选择变化:"..tab:getChildIndex(selectedBtn) )
	end)
	tab:setSelectedButton(btns[2])
	tab:setPosition(1000, 100)
	self:addChild(tab)

end



function MainScene:testToggleButton()
	local btn = ext.ToggleButton.new("UI/frame_add.png","UI/frame_append.png")
	btn.enableTouchScale = true
	btn:setPosition(800,100)
	btn:onChange (function ( event )
		print("状态改变:"..(btn:isSelected() and "true" or "false"))
	end)
	self:add(btn)
end


function MainScene:testButton()
	local btn = ext.Button.new("UI/frame_add.png","UI/frame_append.png","UI/frame_append.png")
	-- btn.enableTouchScale = true
	btn:setPosition(100,100)
	btn:onClick (function ( event )
		print("点击")
		--btn:setEnable(false)
	end)
	btn:onPress (function ( event )
		print("按下")
	end)
	btn:onRealClick (function ( event )
		print("真实点击")
	end)
	btn:onRelease (function ( event )
		print("释放")
	end)
	btn:onReleaseOut (function ( event )
		print("在外部释放")
	end)
	self:add(btn)

	-- local normal =display.newScale9Sprite("UI/btn_green_s.png", 0, 0, cc.size(400,200))
	local normal = CCScale9Sprite:create("UI/btn_green_s.png",cc.rect(0,0,189,78) , cc.rect(80,40,10,10))
	normal:setPreferredSize(cc.size(400,200))
	local press = display.newScale9Sprite("UI/btn_green_m.png", 0, 0, cc.size(400,200))
	local btn2 = ext.Button.new(normal,press)
	btn2.enableTouchScale = true 
	btn2:setPosition(400,120)
	self:add(btn2)
	ui.newTTFLabel({text = "Hello, World", size = 50, align = ui.TEXT_ALIGN_CENTER})
        :addTo(btn2)
end


function MainScene:testImage()
	local img = ext.Image.new("UI/load_bk.jpg")
	img:setAnchorPoint(cc.p(0.5,0.5))
	img:setPosition(cc.p(display.cx,display.cy))
	img:setScaleMode(ext.Image.SCALE_MODE_NO_BORDER, cc.size(display.width,display.height))
	self:addChild(img)	
end


--测试事件---------
function MainScene:testEvent()
	--创建事件分发器
    self.dispatcher = ext.EventDispatcher.new()
    --添加侦听
    self.dispatcher:addEventListener(ext.Event.INIT, self.initHandler, self)
    --抛出事件
    self.dispatcher:dispatchEvent(ext.Event.new(ext.Event.INIT))
    --移动侦听器
    self.dispatcher:removeEventListener(ext.Event.INIT,self.initHandler)
    --再次抛出事件
    self.dispatcher:dispatchEvent(ext.Event.new(ext.Event.INIT))
end
function MainScene:initHandler( evt )
	print("get event type:"..evt.type)
end





function MainScene:onEnter()
end

function MainScene:onExit()
end

return MainScene
